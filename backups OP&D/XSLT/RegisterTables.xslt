<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="NewDataSet">
        <xsl:choose>
            <xsl:when test="count(Table)=0">
                <!-- Blank Statement -->
                <p>[BLANK]</p>
            </xsl:when>
            <xsl:otherwise>
                <!-- Content -->
                <xsl:for-each select="Table">
                    <xsl:variable name="Text1Use"><xsl:value-of select="Text1Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text1Name"><xsl:value-of select="Text1Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text2Use"><xsl:value-of select="Text2Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text2Name"><xsl:value-of select="Text2Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text3Use"><xsl:value-of select="Text3Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text3Name"><xsl:value-of select="Text3Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text4Use"><xsl:value-of select="Text4Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text4Name"><xsl:value-of select="Text4Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text5Use"><xsl:value-of select="Text5Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text5Name"><xsl:value-of select="Text5Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text6Use"><xsl:value-of select="Text6Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text6Name"><xsl:value-of select="Text6Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text7Use"><xsl:value-of select="Text7Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text7Name"><xsl:value-of select="Text7Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text8Use"><xsl:value-of select="Text8Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text8Name"><xsl:value-of select="Text8Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text9Use"><xsl:value-of select="Text9Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text9Name"><xsl:value-of select="Text9Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text10Use"><xsl:value-of select="Text10Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text10Name"><xsl:value-of select="Text10Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number1Use"><xsl:value-of select="Number1Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number1Name"><xsl:value-of select="Number1Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number2Use"><xsl:value-of select="Number2Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number2Name"><xsl:value-of select="Number2Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number3Use"><xsl:value-of select="Number3Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number3Name"><xsl:value-of select="Number3Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number4Use"><xsl:value-of select="Number4Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number4Name"><xsl:value-of select="Number4Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number5Use"><xsl:value-of select="Number5Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number5Name"><xsl:value-of select="Number5Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number6Use"><xsl:value-of select="Number6Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number6Name"><xsl:value-of select="Number6Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number7Use"><xsl:value-of select="Number7Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number7Name"><xsl:value-of select="Number7Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number8Use"><xsl:value-of select="Number8Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number8Name"><xsl:value-of select="Number8Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number9Use"><xsl:value-of select="Number9Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number9Name"><xsl:value-of select="Number9Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number10Use"><xsl:value-of select="Number10Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number10Name"><xsl:value-of select="Number10Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Date1Use"><xsl:value-of select="Date1Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Date1Name"><xsl:value-of select="Date1Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Date2Use"><xsl:value-of select="Date2Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Date2Name"><xsl:value-of select="Date2Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Date3Use"><xsl:value-of select="Date3Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Date3Name"><xsl:value-of select="Date3Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Date4Use"><xsl:value-of select="Date4Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Date4Name"><xsl:value-of select="Date4Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Date5Use"><xsl:value-of select="Date5Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Date5Name"><xsl:value-of select="Date5Name" disable-output-escaping="yes"/></xsl:variable>
                   
                    <table style="width:95%;" class="borders" id="RegisterTable">
                    <thead>
                      <tr>
                        <xsl:if test="$Text1Use='true'"><th class="borders title RegText1"><xsl:copy-of select="$Text1Name" /></th></xsl:if>
                        <xsl:if test="$Text2Use='true'"><th class="borders title RegText2"><xsl:copy-of select="$Text2Name" /></th></xsl:if>
                        <xsl:if test="$Text3Use='true'"><th class="borders title RegText3"><xsl:copy-of select="$Text3Name" /></th></xsl:if>
                        <xsl:if test="$Text4Use='true'"><th class="borders title RegText4"><xsl:copy-of select="$Text4Name" /></th></xsl:if>
                        <xsl:if test="$Text5Use='true'"><th class="borders title RegText5"><xsl:copy-of select="$Text5Name" /></th></xsl:if>
                        <xsl:if test="$Text6Use='true'"><th class="borders title RegText6"><xsl:copy-of select="$Text6Name" /></th></xsl:if>
                        <xsl:if test="$Text7Use='true'"><th class="borders title RegText7"><xsl:copy-of select="$Text7Name" /></th></xsl:if>
                        <xsl:if test="$Text8Use='true'"><th class="borders title RegText8"><xsl:copy-of select="$Text8Name" /></th></xsl:if>
                        <xsl:if test="$Text9Use='true'"><th class="borders title RegText9"><xsl:copy-of select="$Text9Name" /></th></xsl:if>
                        <xsl:if test="$Text10Use='true'"><th class="borders title RegText10"><xsl:copy-of select="$Text10Name" /></th></xsl:if>
                        <xsl:if test="$Number1Use='true'"><th class="borders title RegNumber1"><xsl:copy-of select="$Number1Name" /></th></xsl:if>
                        <xsl:if test="$Number2Use='true'"><th class="borders title RegNumber2"><xsl:copy-of select="$Number2Name" /></th></xsl:if>
                        <xsl:if test="$Number3Use='true'"><th class="borders title RegNumber3"><xsl:copy-of select="$Number3Name" /></th></xsl:if>
                        <xsl:if test="$Number4Use='true'"><th class="borders title RegNumber4"><xsl:copy-of select="$Number4Name" /></th></xsl:if>
                        <xsl:if test="$Number5Use='true'"><th class="borders title RegNumber5"><xsl:copy-of select="$Number5Name" /></th></xsl:if>
                        <xsl:if test="$Number6Use='true'"><th class="borders title RegNumber6"><xsl:copy-of select="$Number6Name" /></th></xsl:if>
                        <xsl:if test="$Number7Use='true'"><th class="borders title RegNumber7"><xsl:copy-of select="$Number7Name" /></th></xsl:if>
                        <xsl:if test="$Number8Use='true'"><th class="borders title RegNumber8"><xsl:copy-of select="$Number8Name" /></th></xsl:if>
                        <xsl:if test="$Number9Use='true'"><th class="borders title RegNumber9"><xsl:copy-of select="$Number9Name" /></th></xsl:if>
                        <xsl:if test="$Number10Use='true'"><th class="borders title RegNumber10"><xsl:copy-of select="$Number10Name" /></th></xsl:if>
                        <xsl:if test="$Date1Use='true'"><th class="borders title RegDate1"><xsl:copy-of select="$Date1Name" /></th></xsl:if>
                        <xsl:if test="$Date2Use='true'"><th class="borders title RegDate2"><xsl:copy-of select="$Date2Name" /></th></xsl:if>
                        <xsl:if test="$Date3Use='true'"><th class="borders title RegDate3"><xsl:copy-of select="$Date3Name" /></th></xsl:if>
                        <xsl:if test="$Date4Use='true'"><th class="borders title RegDate4"><xsl:copy-of select="$Date4Name" /></th></xsl:if>
                        <xsl:if test="$Date5Use='true'"><th class="borders title RegDate5"><xsl:copy-of select="$Date5Name" /></th></xsl:if>
                      </tr>
                    </thead>
                    <tbody>         
                    
                    <xsl:for-each select="../Table1">
                        <tr>
                        <xsl:if test="$Text1Use='true'"><td class="borders RegText1"><xsl:value-of select="Text1" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Text2Use='true'"><td class="borders RegText2"><xsl:value-of select="Text2" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Text3Use='true'"><td class="borders RegText3"><xsl:value-of select="Text3" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Text4Use='true'"><td class="borders RegText4"><xsl:value-of select="Text4" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Text5Use='true'"><td class="borders RegText5"><xsl:value-of select="Text5" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Text6Use='true'"><td class="borders RegText6"><xsl:value-of select="Text6" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Text7Use='true'"><td class="borders RegText7"><xsl:value-of select="Text7" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Text8Use='true'"><td class="borders RegText8"><xsl:value-of select="Text8" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Text9Use='true'"><td class="borders RegText9"><xsl:value-of select="Text9" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Text10Use='true'"><td class="borders RegText10"><xsl:value-of select="Text10" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Number1Use='true'"><td class="borders RegNumber1"><xsl:value-of select="Number1" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Number2Use='true'"><td class="borders RegNumber2"><xsl:value-of select="Number2" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Number3Use='true'"><td class="borders RegNumber3"><xsl:value-of select="Number3" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Number4Use='true'"><td class="borders RegNumber4"><xsl:value-of select="Number4" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Number5Use='true'"><td class="borders RegNumber5"><xsl:value-of select="Number5" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Number6Use='true'"><td class="borders RegNumber6"><xsl:value-of select="Number6" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Number7Use='true'"><td class="borders RegNumber7"><xsl:value-of select="Number7" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Number8Use='true'"><td class="borders RegNumber8"><xsl:value-of select="Number8" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Number9Use='true'"><td class="borders RegNumber9"><xsl:value-of select="Number9" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Number10Use='true'"><td class="borders RegNumber10"><xsl:value-of select="Number10" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Date1Use='true'"><td class="borders RegDate1"><xsl:value-of select="Date1" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Date2Use='true'"><td class="borders RegDate2"><xsl:value-of select="Date2" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Date3Use='true'"><td class="borders RegDate3"><xsl:value-of select="Date3" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Date4Use='true'"><td class="borders RegDate4"><xsl:value-of select="Date4" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Date5Use='true'"><td class="borders RegDate5"><xsl:value-of select="Date5" disable-output-escaping="yes" /></td></xsl:if>
                        </tr>
                    </xsl:for-each>
                    
                    
                      </tbody>
                    </table>
                   
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>