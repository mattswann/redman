<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="NewDataSet">
        <xsl:choose>
            <xsl:when test="count(Table)=0">
                <!-- Blank Statement -->
                <p>[BLANK]</p>
            </xsl:when>
            <xsl:otherwise>
                <!-- Content -->
                <xsl:for-each select="Table">
                    <xsl:variable name="Text2Use"><xsl:value-of select="Text2Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text2Name"><xsl:value-of select="Text2Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number1Use"><xsl:value-of select="Number1Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number1Name"><xsl:value-of select="Number1Name" disable-output-escaping="yes"/></xsl:variable>                 
                    <xsl:variable name="Number2Use"><xsl:value-of select="Number2Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number2Name"><xsl:value-of select="Number2Name" disable-output-escaping="yes"/></xsl:variable>                 
                    <xsl:variable name="Number3Use"><xsl:value-of select="Number3Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number3Name"><xsl:value-of select="Number3Name" disable-output-escaping="yes"/></xsl:variable>                    
                    <xsl:variable name="Number4Use"><xsl:value-of select="Number4Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number4Name"><xsl:value-of select="Number4Name" disable-output-escaping="yes"/></xsl:variable>                    
                    <xsl:variable name="Number5Use"><xsl:value-of select="Number5Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number5Name"><xsl:value-of select="Number5Name" disable-output-escaping="yes"/></xsl:variable>                    
                    <xsl:variable name="Number6Use"><xsl:value-of select="Number6Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number6Name"><xsl:value-of select="Number6Name" disable-output-escaping="yes"/></xsl:variable>        
                   
                    <table style="width:100%;border: 1px solid black;border-collapse: collapse;" class="borders" id="RegisterTable">
                    <thead>
                      <tr style="border: 1px solid black;border-collapse: collapse;">
                        <xsl:if test="$Text2Use='true'"><th class="borders title" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:copy-of select="$Text2Name" /></th></xsl:if>
                        <xsl:if test="$Number1Use='true'"><th class="borders title" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:copy-of select="$Number1Name" /></th></xsl:if>
                        <xsl:if test="$Number2Use='true'"><th class="borders title" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:copy-of select="$Number2Name" /></th></xsl:if>
                        <xsl:if test="$Number3Use='true'"><th class="borders title" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:copy-of select="$Number3Name" /></th></xsl:if>
                        <xsl:if test="$Number4Use='true'"><th class="borders title" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:copy-of select="$Number4Name" /></th></xsl:if>
                        <xsl:if test="$Number5Use='true'"><th class="borders title" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:copy-of select="$Number5Name" /></th></xsl:if>
                        <xsl:if test="$Number6Use='true'"><th class="borders title" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:copy-of select="$Number6Name" /></th></xsl:if>
                      </tr>
                    </thead>
                    <tbody>         
                    
                    <xsl:for-each select="../Table1">
                        <tr style="border: 1px solid black;border-collapse: collapse;">
                        <xsl:if test="$Text2Use='true'"><td class="borders " style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:value-of select="Text2" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Number1Use='true'"><td class="borders" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:if test="Number1 != ''"><xsl:value-of select="format-number(Number1, '#')" disable-output-escaping="yes" /></xsl:if></td></xsl:if>
                        <xsl:if test="$Number2Use='true'"><td class="borders" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:if test="Number2 != ''"><xsl:value-of select="format-number(Number2, '#')" disable-output-escaping="yes" /></xsl:if></td></xsl:if>
                        <xsl:if test="$Number3Use='true'"><td class="borders" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:if test="Number3 != ''"><xsl:value-of select="format-number(Number3, '#')" disable-output-escaping="yes" /></xsl:if></td></xsl:if>
                        <xsl:if test="$Number4Use='true'"><td class="borders" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:if test="Number4 != ''"><xsl:value-of select="format-number(Number4, '#')" disable-output-escaping="yes" /></xsl:if></td></xsl:if>
                        <xsl:if test="$Number5Use='true'"><td class="borders" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:if test="Number5 != ''"><xsl:value-of select="format-number(Number5, '#')" disable-output-escaping="yes" /></xsl:if></td></xsl:if>
                        <xsl:if test="$Number6Use='true'"><td class="borders" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:if test="Number6 != ''"><xsl:value-of select="format-number(Number6, '#')" disable-output-escaping="yes" /></xsl:if></td></xsl:if>
                        </tr>
                    </xsl:for-each>
                    
                      </tbody>
                    </table>
                    <script type="text/javascript">
                    $(document).ready(function() {$('#RegisterTable').kendoGrid({ sortable: true });});
                    </script>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>