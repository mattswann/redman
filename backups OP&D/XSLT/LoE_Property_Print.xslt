<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" indent="yes" />
    <xsl:param name="PropertyAddress"/>
    <xsl:param name="ParcelList"/>
    <xsl:param name="ZoneList"/>
    <xsl:param name="GISLinkURL"/>  
    <xsl:param name="propKey"/>    
  <xsl:template match="/">
      <table>
          <tr>
            <td>
              <strong>Address:</strong>
            </td>
            <td>
              <strong>&#160; <xsl:value-of select="$PropertyAddress" disable-output-escaping="yes"/></strong>
			  <br /><br />			  
			    View more details in  <a href="https://www.sailis.sa.gov.au/products/order/propertyInterestReport/{$propKey}" target="_blank"><img ID="imgm" src="https://www.sailis.sa.gov.au/products/jawr/images/cb3f666fc2294a7ccda61b39ef34a75f0a/images/LSSA_SAILIS_LO.png" style="height:15px;" /></a>
				<br />
            </td>
            <td rowspan="3">&#160;
              <xsl:if test="$ParcelList='-1'">
                <a href="https://maps.google.com/?q={$PropertyAddress}, New Zealand" id="imgGAddr" runat="server" Target="_blank">
                  <img ID="imgm" src="./Components/Images/Map16x16.png" />
                </a>
              </xsl:if>
            </td>
          </tr>
        <tr>
          <td colspan="5">
              <xsl:if test="$ParcelList!='-1'">
			  <br />
			  To view a detailed interactive property map in SAPPA click on the map below <br /><br />
                  <a href="https://test.maps.sa.gov.au/SAPPA/index.aspx?valuations={$propKey}" target="_blank"><img ID="imgm" src="{$GISLinkURL}/GetMap?ParcelList={$ParcelList}" /></a><br />
              </xsl:if>            
          </td>
        </tr>
      </table>
  </xsl:template>
</xsl:stylesheet>