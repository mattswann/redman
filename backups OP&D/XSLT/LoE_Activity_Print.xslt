<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:key name="zGroups" match="ZoneActListResult" use="ActStatPriority"/>
  <xsl:template match="ArrayOfZoneActListResult">
    <xsl:choose>
      <xsl:when test="ZoneActListResult/Activity=''">
        <!-- Blank Statement -->
      </xsl:when>
      <xsl:otherwise>
        
        <h2>
          <span style="font-weight: bold;">Selected Development(s)</span>
        </h2>
    <div>
          <table id="ZoneAct">
            <tbody>
			  <xsl:for-each select="ZoneActListResult">
			   <xsl:if test="not(Activity = preceding-sibling::ZoneActListResult[1]/Activity)">
                <tr>
                  <td>
                    <h4>
					 <xsl:value-of select="Activity" disable-output-escaping="yes"/>
                    </h4>
                  </td>
                </tr>
				 </xsl:if> 
              </xsl:for-each>
            </tbody>
          </table>
        </div>
   
         <font style="font-weight: bold;" class="h3">
          <br />This development may be subject to multiple assessment pathways.  Please review the document below to determine which pathway may be applicable based on the proposed development compliances to standards
        </font> <br />
        
      <br />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>