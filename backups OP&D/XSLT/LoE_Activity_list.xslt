<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:key name="zGroups" match="ZoneActListResult" use="ActStatPriority"/>
  <xsl:template match="ArrayOfZoneActListResult">
    <xsl:choose>
      <xsl:when test="count(ZoneActListResult)=0">
        <!-- Blank Statement -->
        <p>No pathway was found for these development types. Search again for another development</p>
      </xsl:when>
      <xsl:otherwise>
        <div>
          <table id="ZoneAct">
            <thead>
              <tr>
                <th style="width:25%;">
                  <h2>Development</h2>
                </th>
                <th style="width:25%;">
                  <h2>Zone/Overlay</h2>
                </th>
                <th style="width:25%;">
                  <h2>Development Pathway</h2>
                </th>
                <th style="width:25%;">
                  <h2>Reference</h2>
                </th>
              </tr>
            </thead>
            <tbody>
              <xsl:for-each select="ZoneActListResult">
                <tr>
                  <td>
                    <strong>
                      <xsl:value-of select="Activity" disable-output-escaping="yes"/>
                    </strong>
                  </td>
                  <td>
                    <div style="display:inline-block;">
                      <xsl:value-of select="ZoneName" disable-output-escaping="yes"/>
                    </div>
                  </td>
                  <td>
                    <div style="display:inline-block;color:{HtmlColor}">
                      <xsl:value-of select="ActivityStatus" disable-output-escaping="yes"/>
                    </div>
                  </td>
                  <td>
                    <div style="display:inline-block;">
                      <xsl:value-of select="PolicyReference" disable-output-escaping="yes"/>
                    </div>
                  </td>
                </tr>
              </xsl:for-each>
            </tbody>
          </table>
        </div>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>