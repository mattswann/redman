<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:key name="zGroups" match="ZoneActListResult" use="ActStatPriority"/>
  <xsl:template match="ArrayOfZoneActListResult">
    <xsl:choose>
      <xsl:when test="ZoneActListResult/Activity=''">
        <!-- Blank Statement -->
        <p>No Activities found for this address. Search again for another activity</p>
      </xsl:when>
      <xsl:otherwise>
        
        <div>
          <table id="ZoneAct">
            <thead>
              <tr>
                <th style="width:35%;">
                  <h2>Activity</h2>
                </th>
                <th style="width:35%;">
                  <h2>Zone/Overlay</h2>
                </th>
                <th style="">
                  <h2>Permissibility</h2>
                </th>
              </tr>
            </thead>
            <tbody>
              <xsl:for-each select="ZoneActListResult">
                <tr>
                  <td>
                    <strong>
                      <xsl:value-of select="Activity" disable-output-escaping="yes"/>
                    </strong>
                  </td>
                  <td>
                    <div style="display:inline-block;">
                      <xsl:value-of select="ZoneName" disable-output-escaping="yes"/>
                    </div>
                  </td>
                  <td>
                    <div style="display:inline-block;color:{HtmlColor}">
                      <xsl:value-of select="ActivityStatus" disable-output-escaping="yes"/>
                    </div>
                  </td>
                </tr>
              </xsl:for-each>
            </tbody>
          </table>
        </div>
        <font class="h4" style="font-weight: bold;">
          The resulting Development Pathway of the proposed development type(s) is
        </font> <br />
          <xsl:for-each select="ZoneActListResult">
            <xsl:sort select="ActStatPriority" data-type="number" order="descending"/>
            <xsl:if test="position() = 1">
              <font  class="h4" style="font-weight: bold; color:{HtmlColor}">
                <xsl:value-of select="ActivityStatus" />
              </font>
            <br />            
               <font class="h5" >
                <xsl:value-of select="ActStatText" />
              </font>
            </xsl:if>
          </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>