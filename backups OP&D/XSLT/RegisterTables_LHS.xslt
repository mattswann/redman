<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="NewDataSet">
        <xsl:choose>
            <xsl:when test="count(Table)=0">
                <!-- Blank Statement -->
                <p>[BLANK]</p>
            </xsl:when>
            <xsl:otherwise>
                <!-- Content -->
                <xsl:for-each select="Table">
                    <xsl:variable name="Text1Use"><xsl:value-of select="Text1Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text1Name"><xsl:value-of select="Text1Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text2Use"><xsl:value-of select="Text2Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text2Name"><xsl:value-of select="Text2Name" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text3Use"><xsl:value-of select="Text3Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text3Name"><xsl:value-of select="Text3Name" disable-output-escaping="yes"/></xsl:variable>                 
                    <xsl:variable name="Number1Use"><xsl:value-of select="Number1Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Number1Name"><xsl:value-of select="Number1Name" disable-output-escaping="yes"/></xsl:variable>
                   
                    <table style="width:100%;border: 1px solid black;border-collapse: collapse;" class="borders" id="RegisterTable">
                    <thead>
                      <tr style="border: 1px solid black;border-collapse: collapse;">
                        <xsl:if test="$Text1Use='true'"><th class="borders title RegText1" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:copy-of select="$Text1Name" /></th></xsl:if>
                        <xsl:if test="$Text2Use='true'"><th class="borders title RegText2" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:copy-of select="$Text2Name" /></th></xsl:if>
                        <xsl:if test="$Text3Use='true'"><th class="borders title RegText3" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:copy-of select="$Text3Name" /></th></xsl:if>
                        <xsl:if test="$Number1Use='true'"><th class="borders title RegNumber1" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:copy-of select="$Number1Name" /></th></xsl:if>
                      </tr>
                    </thead>
                    <tbody>         
                    
                    <xsl:for-each select="../Table1">
                        <tr style="border: 1px solid black;border-collapse: collapse;">
                        <xsl:if test="$Text1Use='true'"><td class="borders RegText1" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:value-of select="Text1" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Text2Use='true'"><td class="borders RegText2" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:value-of select="Text2" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Text3Use='true'"><td class="borders RegText3" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:value-of select="Text3" disable-output-escaping="yes" /></td></xsl:if>
                        <xsl:if test="$Number1Use='true'"><td class="borders RegNumber1" style="border: 1px solid black;border-collapse: collapse;;padding:5px;"><xsl:value-of select="format-number(Number1, '#')" disable-output-escaping="yes" /></td></xsl:if>
                        </tr>
                    </xsl:for-each>
                    
                      </tbody>
                    </table>
                    <script type="text/javascript">
                    $(document).ready(function() {$('#RegisterTable').kendoGrid({ sortable: true });});
                    </script>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>