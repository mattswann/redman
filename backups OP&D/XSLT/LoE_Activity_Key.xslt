<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:key name="zGroups" match="ZoneActListResult" use="ActStatPriority"/>
  <xsl:template match="NewDataSet">
    <xsl:choose>
      <xsl:when test="count(Table)=0">
	  <span class="LoeActivityInfo">
          <h6>Use the search tool to find specific development/activities you may be considering.</h6>
<h6>Once you have selected a development and clicked apply, allow the system time to retrieve further information (this may take a few moments). <br />
<span  style="font-style: italic;">If your development is not found then it's not defined and is will be Performance Assessed. Please contact your local Council for more information. <br />Tip: If you can't find a development/activity, try using a similar term.</span></h6>
<!--Click on the help button for further information. -->
<br />
<br /></span>
      </xsl:when>
      <xsl:otherwise>
	  <span class="LoeActivityInfo">
          <br /><h6>Based on the property selected you may be considering the following Development(s):</h6><br/>
		  You can click on a Development/Activity in the list or use the search tool to find specific activities. Results may be more accurate if you use the search tool to find the development/activity that is the best match.<br /><br />

Once a Development is selected, allow the system time to retrieve further information (this may take a few moments). <br />
<span class="LoeHelp" style="font-size:0.8em; font-style: italic;">If your Development is not found then it's not defined, and is controlled, please contact Council.  <br />If you can't find an activity, try using a similar term.</span>
<!--Click on the help button for further information. -->
<br />
<br />
<strong>Select Your Development:</strong><br /></span>

        <div class="clearfix">&#160;</div>
        
        <xsl:for-each select="Table/ParentTitle[not(.=preceding::*)]">
          <xsl:variable name="childContent">
              <xsl:call-template name="ActivityItems">
                <xsl:with-param name="pTitle">
                  <xsl:value-of select="."/>
                </xsl:with-param>
              </xsl:call-template>
          </xsl:variable>
            
          <div class="keyActivity" style="float:left; padding-left:10px; max-width: 250px; min-width: 200px">
            <strong><xsl:value-of select="."/></strong>
            <ul>              
              <xsl:copy-of select="$childContent" />            
            </ul>
          </div>
        </xsl:for-each>
        <div class="clearfix">&#160;</div><br /><br />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

 <xsl:template name="ActivityItems">
    <xsl:param name="pTitle"/>
      <xsl:for-each select="../../Table[ParentTitle=$pTitle]">
              <li style="margin-bottom: 1px"><span onclick="addKeyActivity({ActID},'{Activity}'); return false;"  style="cursor: pointer; font-size:0.8em;">
                <xsl:value-of select="Activity" disable-output-escaping="yes"/>
              </span></li>
      </xsl:for-each>

  </xsl:template>


</xsl:stylesheet>