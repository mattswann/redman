<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:key name="headers" match="Table" use="SectionID"/>
  <xsl:key name="numberHeaders" match="Table" use="SectionID"/>
  <xsl:template match="NewDataSet">
    <xsl:choose>
      <xsl:when test="count(Table)=0"></xsl:when>
      <xsl:otherwise>
        <!-- Content -->
        <xsl:for-each select="Table">
			<xsl:variable name="TermType">
				<xsl:value-of select="TermType" disable-output-escaping="yes"/>
			</xsl:variable>
			<xsl:variable name="xslTemplateName">
					  <xsl:choose>
						<xsl:when test="$TermType='d'">
						  <xsl:call-template name="tableGeneralDefinitionContent">
							<xsl:with-param name="tableParentVal">
							  <xsl:value-of select="term"/>
							</xsl:with-param>
						  </xsl:call-template>
						</xsl:when>
						
						<xsl:otherwise></xsl:otherwise>
					  </xsl:choose>
					</xsl:variable>		  
<xsl:if test="(position()>1)">,</xsl:if>{"term":"<xsl:value-of select="term" disable-output-escaping="yes"/>", "description":"<xsl:copy-of select="$xslTemplateName" />","termtype":"<xsl:value-of select="TermType" disable-output-escaping="yes"/>"}
        </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  
  
  <xsl:template name="tableGeneralDefinitionContent">
    <xsl:param name="tableParentVal"/>	
    <xsl:choose>
      <xsl:when test="count(../Table1[term=$tableParentVal and ScopeType='General'])>0">
		<![CDATA[<div class='clearfix'>&#160;</div>
	
		<table class='RenderTable {RenderTag}Table'>
			<tr>		
					<th class='RenderHeaderCell'>Land Use Term<br />(Column A)</th>
					<th class='RenderHeaderCell' style='width:40%'>Definition<br />(Column B)</th>
					<th class='RenderHeaderCell'>Includes<br />(Column C)</th>
					<th class='RenderHeaderCell'>Excludes<br />(Column D)</th>
			</tr>]]>
				<xsl:for-each select="../Table1[term=$tableParentVal and ScopeType='General']">         
	<![CDATA[<tr>		 
					<td class='RenderCell {RenderTag}'>]]><xsl:value-of select="term" disable-output-escaping="yes"/><![CDATA[</td>
					<td class='RenderCell {RenderTag}'>]]><xsl:value-of select="Description" disable-output-escaping="yes"/><![CDATA[</td>
					<td class='RenderCell {RenderTag}'>]]><xsl:value-of select="IncludeDescription" disable-output-escaping="yes"/><![CDATA[</td>
					<td class='RenderCell {RenderTag}'>]]><xsl:value-of select="ExcludeDescription" disable-output-escaping="yes"/><![CDATA[</td>
			</tr>]]>
			</xsl:for-each>
		 <![CDATA[</table>]]>       
      </xsl:when>
      <xsl:when test="count(../Table1[term=$tableParentVal and ScopeType='Admin'])>0">
		<![CDATA[<div class='clearfix'>&#160;</div>
	
		<table class='RenderTable {RenderTag}Table'>
			<tr>		
					<th class='RenderHeaderCell'>Term<br />(Column A)</th>
					<th class='RenderHeaderCell' style='width:40%'>Definition<br />(Column B)</th>
					<th class='RenderHeaderCell'>Illustrations<br />(Column C)</th>
			</tr>]]>
				<xsl:for-each select="../Table1[term=$tableParentVal and ScopeType='Admin']">         
	<![CDATA[<tr>		 
					<td class='RenderCell {RenderTag}'>]]><xsl:value-of select="term" disable-output-escaping="yes"/><![CDATA[</td>
					<td class='RenderCell {RenderTag}'>]]><xsl:value-of select="Description" disable-output-escaping="yes"/><![CDATA[</td>
					<td class='RenderCell {RenderTag}'>]]><xsl:value-of select="IncludeDescription" disable-output-escaping="yes"/><![CDATA[</td>
			</tr>]]>
			</xsl:for-each>
		 <![CDATA[</table>]]>       

	  </xsl:when>
      <xsl:otherwise>
      </xsl:otherwise>
	  
    </xsl:choose>
  </xsl:template>


</xsl:stylesheet>