<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="NewDataSet">
        <xsl:choose>
            <xsl:when test="count(Table)=0">
                <!-- Blank Statement -->
                <p>[BLANK]</p>
            </xsl:when>
            <xsl:otherwise>
                <!-- Content -->
                <xsl:for-each select="Table">
                    <xsl:variable name="Text1Use"><xsl:value-of select="Text1Use" disable-output-escaping="yes"/></xsl:variable>
                    <xsl:variable name="Text1Name"><xsl:value-of select="Text1Name" disable-output-escaping="yes"/></xsl:variable>
                   
                    <table style="width:100%;border: 1px solid black;border-collapse: collapse;" class="borders" id="RegisterTable">
                    <thead>
                      <tr style="border: 1px solid black;border-collapse: collapse;">
                        <xsl:if test="$Text1Use='true'"><th class="borders title RegText1" style="border: 1px solid black;border-collapse: collapse;padding:5px;"><xsl:copy-of select="$Text1Name" /></th></xsl:if>
                      </tr>
                    </thead>
                    <tbody>         
                    
                    <xsl:for-each select="../Table1">
                        <tr style="border: 1px solid black;border-collapse: collapse;">
                        <xsl:if test="$Text1Use='true'"><td class="borders RegText1" style="border: 1px solid black;border-collapse: collapse;padding:5px;"><xsl:value-of select="Text1" disable-output-escaping="yes" /></td></xsl:if>
                        </tr>
                    </xsl:for-each>
                    
                      </tbody>
                    </table>
                    
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>