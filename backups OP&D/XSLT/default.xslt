<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:key name="headers" match="Table" use="SectionID"/>
    <xsl:key name="numberHeaders" match="Table" use="SectionID"/>
    <xsl:param name="isSpecialActStatus"/>
    <xsl:template match="NewDataSet">
        <xsl:choose>
            <xsl:when test="count(Table)=0">
                <!-- Blank Statement -->
                <p>[BLANK]</p>
            </xsl:when>
            <xsl:otherwise>
                <!-- Content -->
                <xsl:for-each select="Table[count(. | key('headers',SectionID)[1]) = 1]">
                    <xsl:variable name="xslTemplateType">
                        <xsl:value-of select="xslTemplateType" disable-output-escaping="yes"/>
                    </xsl:variable>
                    <xsl:variable name="ExtSource">
                        <xsl:value-of select="ExternalSource" disable-output-escaping="yes"/>
                    </xsl:variable>
                    <div style="margin-left:0px;" class="PolicySection">
                            <xsl:value-of select="BreadCrumb" disable-output-escaping="yes"/>
                        <xsl:if test="showHeader!='n' and RenderTagID &gt; -1">
                            <!-- Dont show the header in certain circumstances -->
                            <a name="TOCt_h{SectionID}_ID" id="TOCt_h{SectionID}_ID" class="bookmark">
                                <xsl:choose>
                                    <xsl:when test="DocLevel=1">
                                        <h1 class="headingTitle{SectionID} {RenderTag} Header{DocLevel}" style="margin-top:10px;">
                                            <xsl:if test="showNumberHeader!='n' and SectionNumber!=''">
                                                <xsl:value-of select="SectionNumber" disable-output-escaping="yes"/>
                                                <xsl:text> </xsl:text>
                                            </xsl:if>
                                            <xsl:value-of select="SectionTitle" disable-output-escaping="yes"/>
                                        </h1>
                                    </xsl:when>
                                    <xsl:when test="DocLevel=2">
                                        <h2 class="headingTitle{SectionID} {RenderTag} Header{DocLevel}" style="margin-top:10px;">
                                            <xsl:if test="showNumberHeader!='n' and SectionNumber!=''">
                                                <xsl:value-of select="SectionNumber" disable-output-escaping="yes"/>
                                                <xsl:text> </xsl:text>
                                            </xsl:if>
                                            <xsl:value-of select="SectionTitle" disable-output-escaping="yes"/>
                                        </h2>
                                    </xsl:when>
                                    <xsl:when test="DocLevel=3">
                                        <h3 class="headingTitle{SectionID} {RenderTag} Header{DocLevel}" style="margin-top:10px;">
                                            <xsl:if test="showNumberHeader!='n' and SectionNumber!=''">
                                                <xsl:value-of select="SectionNumber" disable-output-escaping="yes"/>
                                                <xsl:text> </xsl:text>
                                            </xsl:if>
                                            <xsl:value-of select="SectionTitle" disable-output-escaping="yes"/>
                                        </h3>
                                    </xsl:when>
                                    <xsl:when test="DocLevel=4">
                                        <h4 class="headingTitle{SectionID} {RenderTag} Header{DocLevel}" style="margin-top:10px;">
                                            <xsl:if test="showNumberHeader!='n' and SectionNumber!=''">
                                                <xsl:value-of select="SectionNumber" disable-output-escaping="yes"/>
                                                <xsl:text> </xsl:text>
                                            </xsl:if>
                                            <xsl:value-of select="SectionTitle" disable-output-escaping="yes"/>
                                        </h4>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <h5 class="headingTitle{SectionID} {RenderTag} Header5" style="margin-top:10px;">
                                            <xsl:if test="showNumberHeader!='n' and SectionNumber!=''">
                                                <xsl:value-of select="SectionNumber" disable-output-escaping="yes"/>
                                                <xsl:text> </xsl:text>
                                            </xsl:if>
                                            <xsl:value-of select="SectionTitle" disable-output-escaping="yes"/>
                                        </h5>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </a>              
                        </xsl:if>
                        <div style="margin-left:0px; padding:0px;">            
                            <xsl:for-each select="key('headers', SectionID)">

                                <xsl:choose>
                                    <xsl:when test="RenderTagID &lt; 0">
                                        <P24Reg sectionID="{SectionID}" RegisterName="{SectionTitle}" />
                                    </xsl:when> 
                                    <xsl:otherwise>
                                        <xsl:if test="TblType='None' or TblType='Cntn'">    
                                            <div class="PolicyDocContent {RenderTag}">             
                                                <xsl:value-of select="DocContent" disable-output-escaping="yes"/>    
                                            </div>             
                                        </xsl:if>        
                                        <xsl:if test="DynamicContent='true'">    

                                            <div class="PolicyDocContent {RenderTag}">             
                                                <xsl:value-of select="DocContent" disable-output-escaping="yes"/>    
                                            </div>             

                                            <xsl:variable name="DefTemplateName">
                                                <xsl:choose>
                                                    <xsl:when test="$ExtSource='General Definition'">
                                                        <xsl:call-template name="tableGeneralDefinitionContent">                                
                                                        </xsl:call-template>
                                                    </xsl:when>
                                                    <xsl:when test="$ExtSource='Admin Definition'">
                                                        <xsl:call-template name="tableAdminDefinitionContent">                                
                                                        </xsl:call-template>
                                                    </xsl:when>
                                                    <xsl:otherwise/>
                                                </xsl:choose>
                                            </xsl:variable>

                                            <xsl:copy-of select="$DefTemplateName" />
                                        </xsl:if>        
                                        <xsl:if test="TblType='Start'">
                                            <div class="PolicyDocContent">    
                                                <div id="ContentTable{SectionID}" class="divContentTable">
                                                    <xsl:variable name="xslTemplateName">
                                                        <xsl:choose>
                                                            <xsl:when test="$xslTemplateType='General'">
                                                                <xsl:call-template name="GeneralContent">
                                                                    <xsl:with-param name="ParentID">
                                                                        <xsl:value-of select="RMID"/>
                                                                    </xsl:with-param>
                                                                    <xsl:with-param name="qPosition">
                                                                        <xsl:value-of select="position()"/>
                                                                    </xsl:with-param>
                                                                </xsl:call-template>
                                                            </xsl:when>
                                                            <xsl:when test="$xslTemplateType='StandardTable'">
                                                                <xsl:call-template name="tableContent">
                                                                    <xsl:with-param name="tableParentVal">
                                                                        <xsl:value-of select="SectionID"/>
                                                                    </xsl:with-param>
                                                                </xsl:call-template>
                                                            </xsl:when>
                                                            <xsl:when test="($xslTemplateType='AssessmentTable' or $xslTemplateType='AssessmentTableNoSub' or $xslTemplateType='AssessmentTableTwoSub') and ($isSpecialActStatus='0' or $isSpecialActStatus='')">
                                                                <xsl:call-template name="tableAssessmentContent">
                                                                    <xsl:with-param name="tableParentVal">
                                                                        <xsl:value-of select="SectionID"/>
                                                                    </xsl:with-param>
                                                                </xsl:call-template>
                                                            </xsl:when>
                                                            <xsl:when test="($xslTemplateType='AssessmentTable' or $xslTemplateType='AssessmentTableNoSub' or $xslTemplateType='AssessmentTableTwoSub') and ($isSpecialActStatus='1' or $isSpecialActStatus='')">
                                                                <xsl:call-template name="tableSingleColAssessmentContent">
                                                                    <xsl:with-param name="tableParentVal">
                                                                        <xsl:value-of select="SectionID"/>
                                                                    </xsl:with-param>
                                                                </xsl:call-template>
                                                            </xsl:when>
                                                            <xsl:when test="$xslTemplateType='SingleColumn'">
                                                                <xsl:call-template name="tableSingleColumnContent">
                                                                    <xsl:with-param name="tableParentVal">
                                                                        <xsl:value-of select="SectionID"/>
                                                                    </xsl:with-param>
                                                                </xsl:call-template>
                                                            </xsl:when>
                                                            <xsl:otherwise/>
                                                        </xsl:choose>
                                                    </xsl:variable>

                                                    <xsl:copy-of select="$xslTemplateName" />
                                                </div>
                                            </div>    
                                        </xsl:if>             

                                    </xsl:otherwise>

                                </xsl:choose>

                            </xsl:for-each>
                        </div>

                    </div>

                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


    <xsl:template name="GeneralContent">
        <xsl:param name="tableParentVal"/>
        <div class="clearfix">&#160;</div>

        <xsl:for-each select="../Table1[Parent_ID=$tableParentVal and virtualNode='false']">
            <xsl:sort select="tableRow"/>

            <div class="PolicyDocContent {RenderTag}">             
                <xsl:value-of select="DocContent" disable-output-escaping="yes"/>    
                <xsl:variable name="tblCellChildContent">
                    <xsl:call-template name="tableVirtualContent">
                        <xsl:with-param name="tableParentVal">
                            <xsl:value-of select="SectionID"/>
                        </xsl:with-param>                             
                    </xsl:call-template>
                </xsl:variable>
                <xsl:copy-of select="$tblCellChildContent" />
            </div>             
        </xsl:for-each>

    </xsl:template>

    <xsl:template name="tableContent">
        <xsl:param name="tableParentVal"/>
        <div class="clearfix">&#160;</div>
        <table class="RenderTable {RenderTag}Table">
            <tr>
                <xsl:for-each select="../Table1[Parent_ID=$tableParentVal and TblType='Column']">
                    <th class='RenderHeaderCell {RenderTag} THLevel{SubLevel}'>
                        <xsl:value-of select="SectionTitle" disable-output-escaping="yes"/>
                    </th>
                </xsl:for-each>
                <xsl:for-each select="../Table1[Parent_ID=$tableParentVal and TblType='Cell' and virtualNode='false']">
                    <xsl:sort select="tableRow"/>
                    <xsl:if test="(not(preceding::Table1[1]/tableRow=tableRow) or position()=1)">
                        <![CDATA[</tr><tr>]]>
                    </xsl:if>

                    <td class="RenderCell {RenderTag}">
                        <xsl:value-of select="DocContent" disable-output-escaping="yes"/>
                        <xsl:variable name="tblCellChildContent">
                            <xsl:call-template name="tableVirtualContent">
                                <xsl:with-param name="tableParentVal">
                                    <xsl:value-of select="SectionID"/>
                                </xsl:with-param>                             
                            </xsl:call-template>
                        </xsl:variable>
                        <xsl:copy-of select="$tblCellChildContent" />
                    </td>
                </xsl:for-each>


            </tr>
        </table>
    </xsl:template>

    <xsl:template name="tableSingleColumnContent">
        <xsl:param name="tableParentVal"/>
        <div class="clearfix">&#160;</div>
        <table class="RenderTable {RenderTag}Table">
            <tr>        
                <th class="RenderHeaderCell" colspan="2">Desired Outcome</th>
            </tr>
            <xsl:for-each select="../Table1[Parent_ID=$tableParentVal and virtualNode='false']">
                <xsl:sort select="tableRow"/>
                <tr>               
                    <td style="width:10%" class="RenderCell {RenderTag}">
						<xsl:value-of select="SectionTitle" disable-output-escaping="yes"/>
					</td>
                    <td class="RenderCell {RenderTag}">
                        <xsl:value-of select="DocContent" disable-output-escaping="yes"/>
                        <xsl:variable name="tblCellChildContent">
                            <xsl:call-template name="tableVirtualContent">
                                <xsl:with-param name="tableParentVal">
                                    <xsl:value-of select="SectionID"/>
                                </xsl:with-param>                             
                            </xsl:call-template>
                        </xsl:variable>
                        <xsl:copy-of select="$tblCellChildContent" />
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>

    <xsl:template name="tableAssessmentContent">
        <xsl:param name="tableParentVal"/>
        <div class="clearfix">&#160;</div>
        
        <table class="RenderTable {RenderTag}Table">
            <tr>        
                <th class="RenderHeaderCell" style="width:50%">Performance Outcome</th>
                <th class="RenderHeaderCell" style="width:50%">Deemed to Satisfy</th>
            </tr>
            <tr>

                <xsl:for-each select="../Table1[Parent_ID=$tableParentVal and virtualNode='false']">
                    <xsl:sort select="ordercol"/>
                    <xsl:choose>
                        <xsl:when test="TblType='Column'">        
                            <th class="RenderHeaderCell {RenderTag} THLevel{SubLevel}" colspan="2">
                                <xsl:value-of select="SectionTitle" disable-output-escaping="yes"/>
                            </th>
                            <![CDATA[</tr><tr>]]>                
                        </xsl:when>
                        <xsl:when test="TblType='Cell'">        
                            <td class="RenderCell {RenderTag}" style="width:50%">
                                <h5>
                                    <xsl:value-of select="SectionTitle" disable-output-escaping="yes"/>
                                </h5>
                                <xsl:value-of select="DocContent" disable-output-escaping="yes"/>
                                <xsl:variable name="tblCellChildContent">
                                    <xsl:call-template name="tableVirtualContent">
                                        <xsl:with-param name="tableParentVal">
                                            <xsl:value-of select="SectionID"/>
                                        </xsl:with-param>                             
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:copy-of select="$tblCellChildContent" />
                            </td>
                        </xsl:when>
                    </xsl:choose>
                    <xsl:if test="TblType='Column' or OddEven=0">
                        <![CDATA[</tr><tr>]]>                    
                    </xsl:if>
                </xsl:for-each>
            </tr>
        </table>
    </xsl:template>  

    <xsl:template name="tableSingleColAssessmentContent">
        <xsl:param name="tableParentVal"/>
        <div class="clearfix">&#160;</div>
        <table class="RenderTable {RenderTag}Table">
            <tr>        
                <th class="RenderHeaderCell">Deemed to Satisfy</th>
            </tr>

                <xsl:for-each select="../Table1[Parent_ID=$tableParentVal and virtualNode='false']">
                    <xsl:sort select="ordercol"/>
            <tr>
                    <xsl:choose>
                        <xsl:when test="TblType='Column'">        
                            <th class="RenderHeaderCell {RenderTag} THLevel{SubLevel}">
                                <xsl:value-of select="SectionTitle" disable-output-escaping="yes"/>
                            </th>              
                        </xsl:when>
                        <xsl:when test="TblType='Cell'">        
                            <td class="RenderCell {RenderTag}">
                                <h5>
                                    <xsl:value-of select="SectionTitle" disable-output-escaping="yes"/>
                                </h5>
                                <xsl:value-of select="DocContent" disable-output-escaping="yes"/>
                                <xsl:variable name="tblCellChildContent">
                                    <xsl:call-template name="tableVirtualContent">
                                        <xsl:with-param name="tableParentVal">
                                            <xsl:value-of select="SectionID"/>
                                        </xsl:with-param>                             
                                    </xsl:call-template>
                                </xsl:variable>
                                <xsl:copy-of select="$tblCellChildContent" />
                            </td>
                        </xsl:when>
                    </xsl:choose>
                    </tr>
                </xsl:for-each>
        </table>
    </xsl:template>

    <xsl:template name="tableVirtualContent">
        <xsl:param name="tableParentVal"/>
        <div class="clearfix">&#160;</div>
        <xsl:for-each select="../Table1[oParentId=$tableParentVal]">
            <xsl:sort select="ordercol"/>
            <xsl:choose>
                <xsl:when test="RenderTagID &lt; 0">
                    <P24Reg sectionID="{SectionID}" RegisterName="{SectionTitle}" />
                </xsl:when> 
                <xsl:otherwise>
                    <xsl:value-of select="DocContent" disable-output-escaping="yes"/>                            
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>


    <xsl:template name="tableGeneralDefinitionContent">
        <div class="clearfix">&#160;</div>
        <table class="RenderTable {RenderTag}Table">
            <tr>        
                <th class="RenderHeaderCell">Land Use Term<br />(Column A)</th>
                <th class="RenderHeaderCell" style="width:40%">Definition<br />(Column B)</th>
                <th class="RenderHeaderCell">Includes<br />(Column C)</th>
                <th class="RenderHeaderCell">Excludes<br />(Column D)</th>
            </tr>
            <xsl:for-each select="../Table2">         
                <tr>         
                    <td class="RenderCell {RenderTag}">
                        <xsl:value-of select="DefinTitle" disable-output-escaping="yes"/>
                    </td>
                    <td class="RenderCell {RenderTag}">
                        <xsl:value-of select="Description" disable-output-escaping="yes"/>
                    </td>
                    <td class="RenderCell {RenderTag}">
                        <xsl:value-of select="IncludeDescription" disable-output-escaping="yes"/>
                    </td>
                    <td class="RenderCell {RenderTag}">
                        <xsl:value-of select="ExcludeDescription" disable-output-escaping="yes"/>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>

    <xsl:template name="tableAdminDefinitionContent">
        <div class="clearfix">&#160;</div>
        <table class="RenderTable {RenderTag}Table">
            <tr>        
                <th class="RenderHeaderCell">Term<br />(Column A)</th>
                <th class="RenderHeaderCell" style="width:40%">Definition<br />(Column B)</th>
                <th class="RenderHeaderCell" style="width:40%">Illustrations<br />(Column C)</th>
            </tr>
            <xsl:for-each select="../Table3">

                <tr>         
                    <td class="RenderCell {RenderTag}">
                        <xsl:value-of select="DefinTitle" disable-output-escaping="yes"/>
                    </td>
                    <td class="RenderCell {RenderTag}">
                        <xsl:value-of select="Description" disable-output-escaping="yes"/>
                    </td>
                    <td class="RenderCell {RenderTag}">
                        <xsl:value-of select="IncludeDescription" disable-output-escaping="yes"/>
                    </td>
                </tr>
            </xsl:for-each>


        </table>
    </xsl:template>


</xsl:stylesheet>
