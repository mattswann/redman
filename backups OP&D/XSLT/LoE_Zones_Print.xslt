<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:key name="zGroups" match="PropZoneListResult" use="GroupType"/>
  <xsl:template match="ArrayOfPropZoneListResult">
    <xsl:choose>
      <xsl:when test="count(PropZoneListResult)=0">
        <!-- Blank Statement -->
        <p>There are no Zones, Sub Zones or Overlays information found for this property.</p>
      </xsl:when>
      <xsl:otherwise>
        <h2>
          <span style="font-weight: bold;">Property Zoning Details</span>
        </h2>
        <table>
          <xsl:for-each select="PropZoneListResult[count(. | key('zGroups',GroupType)[1]) = 1]">
            <!-- Content -->
            <tr>
              <td>
                <strong><xsl:value-of select="GroupType" disable-output-escaping="yes"/></strong>
              </td>
              <td>
                &#160; &#160; &#160;
              </td>
			  <td>
                &#160;
              </td>			  
            </tr>
            <xsl:for-each select="key('zGroups', GroupType)">
              <tr>
                 <td>
                  <strong>&#160;</strong>
                </td>
				<td>
                &#160; &#160; &#160;
              </td>
                <td>
                  <xsl:value-of select="Descr" disable-output-escaping="yes"/>
                </td>
              </tr>
            </xsl:for-each>


          </xsl:for-each>
        </table>
        <br />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>