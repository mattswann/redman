# Read Me - Redman Dynamic Style Sheets

## *Intro*
Because of the firewall limitations and various environments we are going to use a dynamically hosted stylesheet that combines the latest CSS from the Planning Portal and any Redman specific style. 

Once the site is in production this code will be injected from the compiled stylesheet on the planning portal.

The portal style are included as a partial and updated manually - generally each time a change request is authorised. 

Images, icons and logos are also stored on the dynamic host.

Use this link to embed the style sheet 

```html
<link type='text/css' rel='Stylesheet' href='https://portal-style-endpoint.netlify.com/css/main.css' />
```

## Start here.

***Before you start***
You'll need the following

- Node.js v 10 or later
- Gulp and node-sass installed globally


***Getting setup***
Step 1: Clone or download this repo
Step 2: cd into the project
Step 3: run `npm install`
Step 4: use `gulp watch` to start the build tool


## The file structure 
This is a hybrid of the Squiz Matrix SASS structure.
The external systems have been sectioned in into their own folders for easier control

As this is not a PROD build tool the `src` folder also contains the output of the build.

## Commits and Continuous Deployment
Each time a commit is made the updates are deployed from Gitlab to the Netlify instance. 

Commit often and please merge as you go.

If there a build error and the Netlify needs to be restarted please email [matt.swann@sa.gov.au](mailto:matt.swann@sa.gov.au)




