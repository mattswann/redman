var sapp = {
    defaults: {
        cookies: true,
        url: 'https://dpti.sa.gov.au/saplanningportal',
        contactPage: '/contact_us'
    },
    en: {
        'publication-info-title': 'More about this document',
        'read-more': 'read more',
        'read-less': 'view less'
    }
};


// ! function($) {
    $.fn.CreateBanner = function() {
        
        this.init = function(days,title,texthead,text,close,template) {
            var that = this;
            
            if(1==that.debug()) {
                console.log('debug mode: \n\tdays['+ days +']\n\ttitle['+ title +']\n\ttexthead['+ texthead +']\n\ttext['+ text +']\n\tclose['+ close +']\n\ttemplate['+ template +']');
            }
            
            if(that.getUrlVars()["cookie"] && that.getUrlVars()["cookie"]==='false') {
                console.log('Reset cookie, done.');
                var tempTitle = title + "-notify"; 
                Cookies.remove(tempTitle, { path: '/' }); // removed!
                that.createNotification(texthead, text, 'top-banner', days, title, close, template);
            } else if ( !that.cookieExist(title) ){
                if(1==that.debug()) {
                    console.log('debug mode: Banner creation initiated.');
                } 
                that.createNotification(texthead, text, 'top-banner', days, title, close, template);
            }
            return true;
        };
        
        this.debug = function() {
            // return false;
            return true;
        };
        
        this.restrictChars = function(count,string) {
            var that = this;
            if($.isNumeric(count) == false){
                count = 60;
            }
            // remove html but links
            string = string.replace(/<(?!a\s*\/?)[^>]+>/g, '');
            // count on text only
            var regex = /(<([^>]+)>)/ig;
            var charString = string.replace(regex, "");
            var charStringCnt = charString.length;
            var StringCnt = string.length;
            
            if(1==that.debug()) {
                console.log('charStringCnt [' + charStringCnt + ']');
                console.log('StringCnt [' + StringCnt + ']');
                console.log('[full string] ' + string);
                console.log('[capped string (' + count + ')] ' + string.substr(0, count));
            }
            if(charStringCnt<=count){
                // no need to truncate
                return string;
            }
            
            return string.substr(0, StringCnt - (charStringCnt - count));
        }
        
        this.getUrlVars = function()
        {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for(var i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        };
        
        this.closeBanner = function(item, days, title) {
            var that = this;
            $(item).closest('#alert-panel').slideUp();
            if(1==that.debug()) {
                console.log('debug mode: Banner closed (cookie not set with debug enabled. Disable debug and append URL with ?cookie=false to reset cookie and view banner)');
            } else {
                that.setCookie(days,title); // uncomment to set cookie
            }
        };
        
        this.createNotification = function(messagehead, message, type, days, title, close, template) {
            var that = this;
            var theStyle="",theLinkStyle="";
            $.each(template, function(i, val) {
                if(i!='link-color'){
                    theStyle+= i + ":" + val + ";";
                } else {
                    theLinkStyle = {"color":val};
                }
            });
            
            messagehead = that.restrictChars(60,messagehead);
            message = that.restrictChars(120,message);
            
            var html = '<div class="container"><div class="row px-3 px-sm-0 py-3 py-sm-3"><div class="alert alert-panel mb-0 pr-0"><button title="dismiss" type="button" class="pr-0 pt-1 close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="fas fa-times"></i></span></button>';
       
            html += '<p class="alert-title font-weight-bolder mb-2">' + messagehead + '</p>';
            html += '<p class="font-90 alert-message mb-0">' + message + '</p>';
            html += '</div></div></div>';
            
            $(html).prependTo("#alert-panel");
            
            setTimeout(function(){
                $('.page-wrapper #alert-panel').css({"top": "0px","position": "relative","z-index": "999","width": "100%"});
                $('.page-wrapper #alert-panel .close').attr('title',close);
                $('.page-wrapper #alert-panel').slideDown(100);
                if(theLinkStyle!=""){
                    $('.page-wrapper #alert-panel').find('a').css(theLinkStyle);
                }
                
                if(1==that.debug()) {
                    console.log('debug mode: Banner creation completed.');
                }
            }, 100);
            
        };
        
        this.cookieExist = function(title){
        	var that = this;
        	title = title + "-notify";
        	if (Cookies.get(title) === null || 
        			Cookies.get(title) === "" || 
        			Cookies.get(title) === "null" || 
        			Cookies.get(title) === undefined) {
        			/* no cookie found */
                return false;
            } else {
                if(1==that.debug()) {
                    console.log('debug mode: cookie is set, no banner this time.');
                }
                return true;
            }
        };
        
        this.setCookie = function(days, title){
        	var date = new Date();
        	var minutes = 60 * 24 * days; //3 days 
        	date.setTime(date.getTime() + (minutes * 60 * 1000));
        	/* set cookie */
        	title = title + "-notify"; 
        	Cookies.set(title, "close", {expires: days, path: '/'});
        };
        
    };
    
    // check if any banner data
    
    var ckDays = 365,
        ckTitle = "SA-Planning",
        ckMessageHead = "Default banner text",
        ckMessage = "please create a message for this banner.",
        ckClose = "Don't show me this again.",
        ckTemplate = {"background-color":"#9a52a1","color":"#FFF","link-color":"#FFF"};
    
    if(typeof $ckDays==='undefined' && typeof $ckTitle==='undefined' && typeof $ckMessageHead==='undefined' && typeof $ckMessage==='undefined' && typeof $ckClose==='undefined' && typeof $ckTemplate==='undefined'){
        // nothing yet
    }else{
        
        if(typeof $ckDays==='undefined' || !$ckDays || $ckDays<=0 || typeof $ckDays != 'number'){
            var $ckDays = ckDays;
        }
        if(typeof $ckTitle==='undefined' || !$ckTitle || $ckTitle===""){
            var $ckTitle = ckTitle;
        }
        if(typeof $ckMessage==='undefined' || !$ckMessage || $ckMessage===""){
            var $ckMessage = ckMessage;
        }
            
        if(typeof $ckMessageHead==='undefined' || !$ckMessageHead || $ckMessageHead===""){
            var $ckMessageHead = ckMessageHead;
        }
        if(typeof $ckClose==='undefined' || !$ckClose || $ckClose===""){
            var $ckClose = ckClose;
        }
        if(typeof $ckTemplate==='undefined' || !$ckTemplate || $ckTemplate==="" || (typeof $ckTemplate !== 'object' && typeof $ckTemplate !== null)){
            var $ckTemplate = ckTemplate;
        }
        
        var banner = new $.fn.CreateBanner;
        if (!banner.init($ckDays,$ckTitle,$ckMessageHead,$ckMessage,$ckClose,$ckTemplate)){
            if(1==banner.debug()) {
                console.log('debug mode: Banner failed to initiate');
            } 
        }
        $('body').on( 'click', '#alert-panel .close', function(e) {
            banner.closeBanner(this, $ckDays, $ckTitle);
        });

    }
    
// }(jQuery);
// Sticky Plugin v1.0.4 for jQuery
// =============
// Author: Anthony Garand
// Improvements by German M. Bravo (Kronuz) and Ruud Kamphuis (ruudk)
// Improvements by Leonardo C. Daronco (daronco)
// Created: 02/14/2011
// Date: 07/20/2015
// Website: http://stickyjs.com/
// Description: Makes an element on the page stick on the screen as you scroll
//              It will only set the 'top' and 'position' of your element, you
//              might need to adjust the width in some cases.

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {
    var slice = Array.prototype.slice; // save ref to original slice()
    var splice = Array.prototype.splice; // save ref to original slice()

  var defaults = {
      topSpacing: 0,
      bottomSpacing: 0,
      className: 'is-sticky',
      wrapperClassName: 'sticky-wrapper',
      center: false,
      getWidthFrom: '',
      widthFromWrapper: true, // works only when .getWidthFrom is empty
      responsiveWidth: false,
      zIndex: 'inherit'
    },
    $window = $(window),
    $document = $(document),
    sticked = [],
    windowHeight = $window.height(),
    scroller = function() {
      var scrollTop = $window.scrollTop(),
        documentHeight = $document.height(),
        dwh = documentHeight - windowHeight,
        extra = (scrollTop > dwh) ? dwh - scrollTop : 0;

      for (var i = 0, l = sticked.length; i < l; i++) {
        var s = sticked[i],
          elementTop = s.stickyWrapper.offset().top,
          etse = elementTop - s.topSpacing - extra;

        //update height in case of dynamic content
        s.stickyWrapper.css('height', s.stickyElement.outerHeight());

        if (scrollTop <= etse) {
          if (s.currentTop !== null) {
            s.stickyElement
              .css({
                'width': '',
                'position': '',
                'top': '',
                'z-index': ''
              });
            s.stickyElement.parent().removeClass(s.className);
            s.stickyElement.trigger('sticky-end', [s]);
            s.currentTop = null;
          }
        }
        else {
          var newTop = documentHeight - s.stickyElement.outerHeight()
            - s.topSpacing - s.bottomSpacing - scrollTop - extra;
          if (newTop < 0) {
            newTop = newTop + s.topSpacing;
          } else {
            newTop = s.topSpacing;
          }
          if (s.currentTop !== newTop) {
            var newWidth;
            if (s.getWidthFrom) {
                padding =  s.stickyElement.innerWidth() - s.stickyElement.width();
                newWidth = $(s.getWidthFrom).width() - padding || null;
            } else if (s.widthFromWrapper) {
                newWidth = s.stickyWrapper.width();
            }
            if (newWidth == null) {
                newWidth = s.stickyElement.width();
            }
            s.stickyElement
              .css('width', newWidth)
              .css('position', 'fixed')
              .css('top', newTop)
              .css('z-index', s.zIndex);

            s.stickyElement.parent().addClass(s.className);

            if (s.currentTop === null) {
              s.stickyElement.trigger('sticky-start', [s]);
            } else {
              // sticky is started but it have to be repositioned
              s.stickyElement.trigger('sticky-update', [s]);
            }

            if (s.currentTop === s.topSpacing && s.currentTop > newTop || s.currentTop === null && newTop < s.topSpacing) {
              // just reached bottom || just started to stick but bottom is already reached
              s.stickyElement.trigger('sticky-bottom-reached', [s]);
            } else if(s.currentTop !== null && newTop === s.topSpacing && s.currentTop < newTop) {
              // sticky is started && sticked at topSpacing && overflowing from top just finished
              s.stickyElement.trigger('sticky-bottom-unreached', [s]);
            }

            s.currentTop = newTop;
          }

          // Check if sticky has reached end of container and stop sticking
          var stickyWrapperContainer = s.stickyWrapper.parent();
          var unstick = (s.stickyElement.offset().top + s.stickyElement.outerHeight() >= stickyWrapperContainer.offset().top + stickyWrapperContainer.outerHeight()) && (s.stickyElement.offset().top <= s.topSpacing);

          if( unstick ) {
            s.stickyElement
              .css('position', 'absolute')
              .css('top', '')
              .css('bottom', 0)
              .css('z-index', '');
          } else {
            s.stickyElement
              .css('position', 'fixed')
              .css('top', newTop)
              .css('bottom', '')
              .css('z-index', s.zIndex);
          }
        }
      }
    },
    resizer = function() {
      windowHeight = $window.height();

      for (var i = 0, l = sticked.length; i < l; i++) {
        var s = sticked[i];
        var newWidth = null;
        if (s.getWidthFrom) {
            if (s.responsiveWidth) {
                newWidth = $(s.getWidthFrom).width();
            }
        } else if(s.widthFromWrapper) {
            newWidth = s.stickyWrapper.width();
        }
        if (newWidth != null) {
            s.stickyElement.css('width', newWidth);
        }
      }
    },
    methods = {
      init: function(options) {
        return this.each(function() {
          var o = $.extend({}, defaults, options);
          var stickyElement = $(this);

          var stickyId = stickyElement.attr('id');
          var wrapperId = stickyId ? stickyId + '-' + defaults.wrapperClassName : defaults.wrapperClassName;
          var wrapper = $('<div></div>')
            .attr('id', wrapperId)
            .addClass(o.wrapperClassName);

          stickyElement.wrapAll(function() {
            if ($(this).parent("#" + wrapperId).length == 0) {
                    return wrapper;
            }
});

          var stickyWrapper = stickyElement.parent();

          if (o.center) {
            stickyWrapper.css({width:stickyElement.outerWidth(),marginLeft:"auto",marginRight:"auto"});
          }

          if (stickyElement.css("float") === "right") {
            stickyElement.css({"float":"none"}).parent().css({"float":"right"});
          }

          o.stickyElement = stickyElement;
          o.stickyWrapper = stickyWrapper;
          o.currentTop    = null;

          sticked.push(o);

          methods.setWrapperHeight(this);
          methods.setupChangeListeners(this);
        });
      },

      setWrapperHeight: function(stickyElement) {
        var element = $(stickyElement);
        var stickyWrapper = element.parent();
        if (stickyWrapper) {
          stickyWrapper.css('height', element.outerHeight());
        }
      },

      setupChangeListeners: function(stickyElement) {
        if (window.MutationObserver) {
          var mutationObserver = new window.MutationObserver(function(mutations) {
            if (mutations[0].addedNodes.length || mutations[0].removedNodes.length) {
              methods.setWrapperHeight(stickyElement);
            }
          });
          mutationObserver.observe(stickyElement, {subtree: true, childList: true});
        } else {
          if (window.addEventListener) {
            stickyElement.addEventListener('DOMNodeInserted', function() {
              methods.setWrapperHeight(stickyElement);
            }, false);
            stickyElement.addEventListener('DOMNodeRemoved', function() {
              methods.setWrapperHeight(stickyElement);
            }, false);
          } else if (window.attachEvent) {
            stickyElement.attachEvent('onDOMNodeInserted', function() {
              methods.setWrapperHeight(stickyElement);
            });
            stickyElement.attachEvent('onDOMNodeRemoved', function() {
              methods.setWrapperHeight(stickyElement);
            });
          }
        }
      },
      update: scroller,
      unstick: function(options) {
        return this.each(function() {
          var that = this;
          var unstickyElement = $(that);

          var removeIdx = -1;
          var i = sticked.length;
          while (i-- > 0) {
            if (sticked[i].stickyElement.get(0) === that) {
                splice.call(sticked,i,1);
                removeIdx = i;
            }
          }
          if(removeIdx !== -1) {
            unstickyElement.unwrap();
            unstickyElement
              .css({
                'width': '',
                'position': '',
                'top': '',
                'float': '',
                'z-index': ''
              })
            ;
          }
        });
      }
    };

  // should be more efficient than using $window.scroll(scroller) and $window.resize(resizer):
  if (window.addEventListener) {
    window.addEventListener('scroll', scroller, false);
    window.addEventListener('resize', resizer, false);
  } else if (window.attachEvent) {
    window.attachEvent('onscroll', scroller);
    window.attachEvent('onresize', resizer);
  }

  $.fn.sticky = function(method) {
    if (methods[method]) {
      return methods[method].apply(this, slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error('Method ' + method + ' does not exist on jQuery.sticky');
    }
  };

  $.fn.unstick = function(method) {
    if (methods[method]) {
      return methods[method].apply(this, slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method ) {
      return methods.unstick.apply( this, arguments );
    } else {
      $.error('Method ' + method + ' does not exist on jQuery.sticky');
    }
  };
  $(function() {
    setTimeout(scroller, 0);
  });
}));
//@@ Smooth scrolling using jQuery easing

    $('body').on('click','a.js-scroll-trigger[href*="#"]:not([href="#"])', function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 55)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });
    
    //@@ Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#inpage-nav',
        offset: 60
    });
    
    $(".side-nav#inpage-nav .sticky-top").sticky({topSpacing:30});
    

$('table').addClass('table');
$( "body table.table" ).each(function( index ) {
    var thisTableID = $(this).attr('id');
    var thisTableClass = $(this).attr('class');
    var thisTable = $(this).html();
    $(this).before('<!-- Table wrapper --><div class="d-flex align-items-justify bg-white pb-4 px-4"><table class="' + thisTableClass + '" id="' + thisTableID + '">' + thisTable + '</table></div><!-- end table wrapper -->');
    $(this).remove();
});

$('#template597609 #more_about_this_document').prepend('<h2>'+ sapp.en["publication-info-title"] +'</h2>');
var resizeTimer;

$(window).on('resize load', function (e) {
	clearTimeout(resizeTimer);
	resizeTimer = setTimeout(function () {
		var windowWidth = $(window).width();
		if (windowWidth <= 1024) {
			$('#template597609 .media-body a.stretched-link').each(function () {
				$(this).removeClass('stretched-link');
			});
		}
		if (windowWidth > 1024) {
			$('#template597609 .media-body a').each(function () {
				$(this).addClass('stretched-link');
			});
		}
	}, 300);
});

! function($) {
    $.fn.CreateReadMoreLess = function() {
        
        this.debug = function() {
            return false;
            // return true;
        };
        
        this.init = function(ID,action) {
            var that = this;
            $('.full_description').addClass('d-none');
            if(1==that.debug()) {
                console.log('debug mode: \n\tID['+ ID +']\n\taction['+ action +']');
            }
            // if has data attr
            if(1==that.debug()) {
                console.group('Create read more links');
            }
            try{
                $('.short_description').each( function(index){
                    index++;
                    var dataReadMore = $(this).data('read-more'),
                        dataReadLess = $(this).data('read-less'),
                        readMore, readLess;
                    if(dataReadMore && dataReadMore!=''){
                        readMore = dataReadMore;
                    }else{
                        readMore = sapp.en['read-more'];
                    }
                    if(dataReadLess && dataReadLess!=''){
                        readLess = dataReadLess;
                    }else{
                        readLess = sapp.en['read-less'];
                    }
            	    $(this).append('<p class="d-block pb-5 border-bottom"><span class="register-description">'+readMore+' </span></p>');
            	    if(1==that.debug()) {
                        console.log('Step ' + index + '\n\treadMore['+ readMore +']\n\treadLess['+ readLess +']');
                    }
                });
                if(1==that.debug()) {
                    console.groupEnd('Create read more links');
                }
                return true;
            } catch(err){
                console.warn('failed to create read more links: '+ err);
            }
        };
        
        this.toggleItem = function(item,action){
            var that = this;
            if(1==that.debug()) {
                console.group('Toggle links');
            }
            
            switch(item){
                case 'all':
                    // toggle show / hide all
                    action = $(action);
                    if(1==that.debug()) {
                        console.log('item: ' + item);
                    }
                    var theShowText = $(action).data('toggle-text-show'),
                        theHideText = $(action).data('toggle-text-hide'),
                        theContainer = "body " + $(action).data('toggle-container');
                    if(1==that.debug()) {
                        console.log('$(action).text(): ' + $(action).text());
                        console.log('theShowText: ' + theShowText);
                        console.log('theHideText: ' + theHideText);
                    }
        
            	    if($(action).text() == theShowText) {
                        // show all
                        
                        $(theContainer)
                            .find('.full_description')
                            .addClass('d-block')
                            .removeClass('d-none');
                        var readLess = $(theContainer)
                                            .find('.register-description')
                                            .parent()
                                            .parent()
                                            .parent()
                                            .find('.short_description')
                                            .data('read-less');
                        if(1==that.debug()) {
                            console.log('readLess: ' + readLess);
                        }
                        $(theContainer)
                            .find('.register-description')
                            .addClass('register-description-less')
                            .removeClass('register-description')
                            .text(readLess).parent()
                            .removeClass('border-bottom pb-5');
                            
                        $(action).text(theHideText);
                    }else{
                        // hide all
                        $(theContainer)
                            .find('.full_description')
                            .addClass('d-none')
                            .removeClass('d-block');
                        $(theContainer)
                            .find('.register-description-less')
                            .parent()
                            .addClass('border-bottom pb-5');
                        var readMore = $(theContainer)
                                            .find('.register-description-less')
                                            .parent()
                                            .parent()
                                            .parent()
                                            .find('.short_description')
                                            .data('read-more');
                        if(1==that.debug()) {
                            console.log('readMore: ' + readMore);
                        }
                        $(theContainer)
                            .find('.register-description-less')
                            .addClass('register-description')
                            .removeClass('register-description-less')
                            .text(readMore).parent();
                        
                        $(action).text(theShowText);
                    }
                    break;
                default:
                    item = $(item);
                    if(1==that.debug()) {
                        console.log('action: ' + action);
                    }
                    switch(action){
                        
                        case 'show':
                            var dataReadLess = $(item).parent().parent().parent().find('.short_description').data('read-less');
                            $(item).toggleClass('register-description-less register-description')
                                .text(dataReadLess)
                                .parent()
                                .removeClass('border-bottom pb-5')
                                .parent()
                                .next('.full_description')
                                .toggleClass('d-block d-none');
                            break;
                        case 'hide':
                            var dataReadMore = $(item).parent().parent().parent().find('.short_description').data('read-more');
                            $(item).toggleClass('register-description register-description-less')
                                .text(dataReadMore)
                                .parent()
                                .addClass('border-bottom pb-5')
                                .parent()
                                .next('.full_description')
                                .toggleClass('d-none d-block');
                            break;
                    }
                    break;
            }
            if(1==that.debug()) {
                console.groupEnd('Toggle links');
            }
        };
        
    };
    
    var readMoreLess = new $.fn.CreateReadMoreLess;
    
    if(!readMoreLess.init('all','hide')){
        if(1==readMoreLess.debug()) {
            console.warn('debug mode: readMore readLess failed to initiate');
        }
    }
    // toggle show / hide all
    $('.toggle-read-more').on('click',function(){
        readMoreLess.toggleItem('all',this);
    });
    $('body').on('click',' .register-description', function(){
        readMoreLess.toggleItem(this,'show');
    });
    $('body').on('click',' .register-description-less', function(){
        readMoreLess.toggleItem(this,'hide');
    });
    
}(jQuery);
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
function convertToSlug(Text,slug)
{
    if(slug===false){
        return unescape(Text
        .toLowerCase()
        .replace(/[^\w ]+/g,'')
        .replace(/ +/g,'_'));
    }else{
        return unescape(Text
        .replace(/_+/g, ' ')
        .replace(/-+/g, ' '));
    }
}
$("body").on('click keydown', '.toggle-header', function(e) {
    if(e.type === 'keydown' && e.key === 'Enter') {
        e.preventDefault();
        $(this).click();
        
    }else if(e.type === 'click'){
        $(this).toggleClass('is-active').next(".toggle-content").stop().slideToggle(400, function(){
            $(this).parent().find('.toggle-header svg.svg-inline--fa').attr('role','presentation')
        });
    }
    
});
$('body .toggle-container').find('.toggle-header').attr('tabindex','0');


    if($('div.toggle-header').length > 0 ) {
        var attrs = {};
        $('body .toggle-header').find('svg.svg-inline--fa').attr('role','presentation');
        $.each($("div.toggle-header")[0].attributes, function(idx, attr) {
          attrs[attr.nodeName] = attr.nodeValue;
        });
        
        $("div.toggle-header").replaceWith(function() {
          attrs.text = $(this).text();
          return $("<a />", attrs);
        });
        
    }

var menudebug = true;
// $("header .list-group-item.dropdown").has('> a .arrow-down-icon').each(function () {
	
// 	$(this).click(function (event) {
// 		event.preventDefault();
// 		event.stopPropagation();
// 		if(menudebug===true){console.log('top menu clicked', this);}
		
		
		
// 		if($(this).hasClass('selected')){
// 			//close all
// 			$("header .list-group-item.dropdown").removeClass("selected");
// 			$("header .list-group-item.dropdown .dropdown-menu").removeClass("show");
// 			$("header .list-group-item.dropdown a > span.arrow-up-icon").removeClass('arrow-up-icon').addClass('arrow-down-icon');
// 			return false;
// 		} else {
// 			if($(this).find('.sub-menu-level2 > .dropdown-item').length && !$('header  .menu-mobile-on').length){
// 				$('body').click();
// 			} else {
// 				$("header .list-group-item.dropdown").removeClass("selected");
// 				$("header .list-group-item.dropdown .dropdown-menu").removeClass("show");
// 				$("header .list-group-item.dropdown a > span.arrow-up-icon").removeClass('arrow-up-icon').addClass('arrow-down-icon');
// 			}
// 			$("header .list-group-item.dropdown").removeClass("selected");
// 			$(this).addClass('selected');
// 			$(this).find('.dropdown-menu.dropdown-menu-wide').addClass('show');
// 			$(this).find('a > span.arrow-down-icon').removeClass('arrow-down-icon').addClass('arrow-up-icon');
// 		}
		
// 		//select first item in level 2
// 		if($(this).find('.sub-menu-level2 > .dropdown-item').length && !$('header .menu-mobile-on').length){
// 			$(this).find('.sub-menu-level2 > .dropdown-item').get(0).click();
// 		}
// 		setTimeout(function(){ 
// 		    $('nav .arrow-up-icon').find('svg.svg-inline--fa').attr('role','presentation');
//     		$('nav .arrow-down-icon').find('svg.svg-inline--fa').attr('role','presentation');
//             $('nav .mobile-arrow-right-icon').find('svg.svg-inline--fa').attr('role', 'presentation');
// 		}, 300);
		
		
// 		return false;
// 	});
// 	$(this).find('a').click(function (event) {
// 		if(menudebug===true){console.log('em');}
// 		if(!$('header  .menu-mobile-on').length){
// 			event.preventDefault();
// 		}
// 	})
// });

$("header .list-group-item.dropdown").has('.psa-chevron').each(function () {
	
	$(this).click(function (event) {
		event.preventDefault();
		event.stopPropagation();
		if(menudebug===true){console.log('top menu clicked', this);}
		
		
		
		if($(this).hasClass('selected')){
			//close all
			$("header .list-group-item.dropdown").removeClass("selected");
			$("header .list-group-item.dropdown .dropdown-menu").removeClass("show");
			$("header .list-group-item.dropdown a > span.icon-up").removeClass('icon-up').addClass('icon-down');
			return false;
		} else {
			if($(this).find('.sub-menu-level2 > .dropdown-item').length && !$('header  .menu-mobile-on').length){
				$('body').click();
			} else {
				$("header .list-group-item.dropdown").removeClass("selected");
				$("header .list-group-item.dropdown .dropdown-menu").removeClass("show");
				$("header .list-group-item.dropdown a > span.icon-up").removeClass('icon-up').addClass('psa-chevron.icon-down');
			}
			$("header .list-group-item.dropdown").removeClass("selected");
			$(this).addClass('selected');
			$(this).find('.dropdown-menu.dropdown-menu-wide').addClass('show');
			$(this).find('a > span.psa-chevron.icon-down').removeClass('icon-down').addClass('icon-up');
		}
		
		//select first item in level 2
		if($(this).find('.sub-menu-level2 > .dropdown-item').length && !$('header .menu-mobile-on').length){
			$(this).find('.sub-menu-level2 > .dropdown-item').get(0).click();
		}
		setTimeout(function(){ 
            $('nav .arrow-up-icon').find('svg.svg-inline--fa').attr('role','presentation');
            $('nav .psa-chevron.icon-down').find('svg.svg-inline--fa').attr('role','presentation');
            $('nav .mobile-arrow-right-icon').find('svg.svg-inline--fa').attr('role', 'presentation');
		}, 300);
		
		return false;
	});
	$(this).find('a').click(function (event) {
		if(menudebug===true){console.log('em');}
		if(!$('header  .menu-mobile-on').length){
			event.preventDefault();
		}
	})
});


$("header .list-group-item.dropdown .sub-menu-level2 .dropdown-item").each(function () {
	$(this).click(function (event) {
		
		
		if( $('header  .menu-mobile-on').length < 1 ){
			if(menudebug===true){console.log('sub-menu-level2 menu clicked on desktop');}
			event.preventDefault();
			event.stopPropagation();
			
			$('.sub-menu-level2 .dropdown-item').removeClass('selected');
			
			$(this).addClass('selected');
			var toolsSection = $(this).attr('rel');
			if(menudebug===true){console.log('tools section: ' + toolsSection);}
			$('.level1-tools div[data-tools-rel!="' + toolsSection + '"]').addClass('d-none');
			$('.level1-tools div[data-tools-rel="' + toolsSection + '"]').removeClass('d-none');
			
			var level3_container = $(this).parents('.dropdown.selected').find('.sub-menu-level3-container');
			
			level3_container.html('');
			$('nav .arrow-up-icon').find('svg.svg-inline--fa').attr('role','presentation');
    		$('nav .arrow-down-icon').find('svg.svg-inline--fa').attr('role','presentation');
            $('nav .mobile-arrow-right-icon').find('svg.svg-inline--fa').attr('role', 'presentation');
			$(this).find('.sub-menu-level3').clone().appendTo( level3_container );
			$("header .sub-menu-level3-container  a").on('click', function (event) {
				if(menudebug===true){console.log('.sub-menu-level3 a');}
				event.stopPropagation();
				return true;
			})
			return false;
		} else {
			event.stopPropagation();
			if(menudebug===true){console.log('sub-menu-level2 menu clicked on mobile');}
			return true; // follow a link clicked
		}
	});
})



$("header .list-group-item.dropdown .dropdown-menu").click(function () {
	if(menudebug===true){console.log('dropdown-menu');}
	return false;
})


//  CLOSE THE MENU 
$('body, .mbtn-close, .btn-mainmenu-close').click(function () {
	if(menudebug===true){console.log('body clicked');}
	$("header .list-group-item.dropdown").removeClass("selected");
	$("header .list-group-item.dropdown .dropdown-menu").removeClass("show");
	$("header .list-group-item.dropdown a > span.arrow-up-icon").removeClass('arrow-up-icon').addClass('arrow-down-icon');
	$('header nav').removeClass('menu-mobile-on').addClass('d-none');
	$('body .arrow-up-icon').find('svg.svg-inline--fa').attr('role','presentation');
	$('body .arrow-down-icon').find('svg.svg-inline--fa').attr('role','presentation');
    $('nav .mobile-arrow-right-icon').find('svg.svg-inline--fa').attr('role', 'presentation');
	$('.btn-mainmenu-open').removeClass('d-none').addClass('d-block')
	$('.btn-mainmenu-close').hide();
});

$(window).resize(function(){
	if(!is_scrolling){
		$('body').click();
	}
});


// ESCAPE KEY
$(document).keyup(function (e) {
	if(menudebug===true){console.log('escape');}
	var KEYCODE_ESC = 27;
	if (e.keyCode == KEYCODE_ESC) {
		$('body').click();
	}
});


// mobile menu

$('.btn-mainmenu-open').click(function(event){
	if(menudebug===true){console.log('btn-mainmenu-open');}
	event.preventDefault();
	event.stopPropagation();
	$('header nav').removeClass('d-none').addClass('menu-mobile-on');
	//$('header nav .ul-menu').toggleClass('d-flex');
	
	$(this).addClass('d-none').removeClass('d-block')
	$('.btn-mainmenu-close').show();
});

$('.btn-search').click(function(){
	$(this).toggleClass('mobile-searchicon searchicon-close ');
});

// replace Squiz brackets
$('header .list-group-item a').each(function(){
    var linkText = $(this).html();
    linkText = linkText.replace(/\(/g, '');
    linkText = linkText.replace(/\)/g, '');
    $(this).html(linkText);
});



var t, l = (new Date()).getTime();
var is_scrolling = false;

$(window).scroll(function(){
    var now = (new Date()).getTime();
    
    if(now - l > 400){
        $(this).trigger('scrollStart');
        l = now;
    }
    
    clearTimeout(t);
    t = setTimeout(function(){
        $(window).trigger('scrollEnd');
    }, 300);
});

$(window).on('scrollStart', function(){
    if(menudebug===true){console.log('scrollStart');}
	is_scrolling = true;
});

$(window).on('scrollEnd', function(){
    if(menudebug===true){console.log('scrollEnd');}
	is_scrolling = false;
});

jQuery.expr[':'].icontains = function(a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};
if(typeof theSearchTerm != 'undefined' && theSearchTerm.length > 0){
    theSearchTerm = theSearchTerm.trim();
    var theSearchTermExp = new RegExp(theSearchTerm,"gi");
    $(".search-results .search-description p:icontains('" + theSearchTerm + "')").each(function() {
        $( this ).html(
            $(this)
            .html()
            .replace(theSearchTermExp, '<strong>$&</strong>')
        );
    });
}

//  Temp JS for Modal    //

// $('#input-search').on('keypress', function (e) {
// 	if (e.which == 13) {
// 		searchInput();
// 	}
// });

// $('header .search-icon>svg.svg-inline--fa').on('click', function () {
// 	searchInput();
// });

// function searchInput() {
// 	var searchInput = $('#input-search').val();
// 	$('.searchModal').removeClass('d-none').fadeIn('slow').addClass('show').css({'display': 'block'});
// 	$('.overlay').removeClass('d-none').fadeIn('slow').addClass('d-block');
// 	$('.modal-content > .modal-body > .search-value > .textoutput').text(searchInput);
// }

// why do we need this? Bootstrap modal closes with esc key!
// $(document).keyup(function (e) {
// 	var KEYCODE_ESC = 27;
// 	if (e.keyCode == KEYCODE_ESC) {
// 		closeModal();
// 	}
// });


// this should use bootstrap modal events, no need for custom.
// $('.btnYes, .btnNo, .close, .overlay').on('click', function () {
//     closeModal();
// });

// function closeModal() {
// 	$('.searchModal').fadeOut('slow');
// 	$('.overlay').fadeOut('slow').removeClass('d-block');
// }




 /*
*   This content is licensed according to the W3C Software License at
*   https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
*
*   Simple accordion pattern example
*/

'use strict';

Array.prototype.slice.call(document.querySelectorAll('.accordion')).forEach(function (accordion) {

  // Allow for multiple accordion sections to be expanded at the same time
  var allowMultiple = accordion.hasAttribute('data-allow-multiple');
  // Allow for each toggle to both open and close individually
  var allowToggle = (allowMultiple) ? allowMultiple : accordion.hasAttribute('data-allow-toggle');

  // Create the array of toggle elements for the accordion group
  var triggers = Array.prototype.slice.call(accordion.querySelectorAll('.accordion-trigger'));
  var panels = Array.prototype.slice.call(accordion.querySelectorAll('.accordion-panel'));


  accordion.addEventListener('click', function (event) {
    var target = event.target;
    $('body .accordion-trigger').find('svg.svg-inline--fa').attr('role', 'presentation');
    if (target.classList.contains('accordion-trigger')) {
      // Check if the current toggle is expanded.
      var isExpanded = target.getAttribute('aria-expanded') == 'true';
      var active = accordion.querySelector('[aria-expanded="true"]');

      // without allowMultiple, close the open accordion
      if (!allowMultiple && active && active !== target) {
        // Set the expanded state on the triggering element
        active.setAttribute('aria-expanded', 'false');
        // Hide the accordion sections, using aria-controls to specify the desired section
        document.getElementById(active.getAttribute('aria-controls')).setAttribute('hidden', '');

        // When toggling is not allowed, clean up disabled state
        if (!allowToggle) {
          active.removeAttribute('aria-disabled');
        }
      }

      if (!isExpanded) {
        // Set the expanded state on the triggering element
        target.setAttribute('aria-expanded', 'true');
        // Hide the accordion sections, using aria-controls to specify the desired section
        document.getElementById(target.getAttribute('aria-controls')).removeAttribute('hidden');

        // If toggling is not allowed, set disabled state on trigger
        if (!allowToggle) {
          target.setAttribute('aria-disabled', 'true');
        }
      }
      else if (allowToggle && isExpanded) {
        // Set the expanded state on the triggering element
        target.setAttribute('aria-expanded', 'false');
        // Hide the accordion sections, using aria-controls to specify the desired section
        document.getElementById(target.getAttribute('aria-controls')).setAttribute('hidden', '');
      }

      event.preventDefault();
    }
  });

  // Bind keyboard behaviors on the main accordion container
  accordion.addEventListener('keydown', function (event) {
    var target = event.target;
    var key = event.which.toString();

    var isExpanded = target.getAttribute('aria-expanded') == 'false';
    var allowToggle = (allowMultiple) ? allowMultiple : accordion.hasAttribute('data-allow-toggle');



    // 33 = Page Up, 34 = Page Down
    var ctrlModifier = (event.ctrlKey && key.match(/33|34/));

    // Is this coming from an accordion header?
    if (target.classList.contains('accordion-trigger')) {
      // Up/ Down arrow and Control + Page Up/ Page Down keyboard operations
      // 38 = Up, 40 = Down
      if (key.match(/38|40/) || ctrlModifier) {
        var index = triggers.indexOf(target);
        var direction = (key.match(/34|40/)) ? 1 : -1;
        var length = triggers.length;
        var newIndex = (index + length + direction) % length;

        triggers[newIndex].focus();

        event.preventDefault();
      }
      else if (key.match(/35|36/)) {
        // 35 = End, 36 = Home keyboard operations
        switch (key) {
          // Go to first accordion
          case '36':
            triggers[0].focus();
            break;
            // Go to last accordion
          case '35':
            triggers[triggers.length - 1].focus();
            break;
        }
        event.preventDefault();

      }

    }
  });

  // These are used to style the accordion when one of the buttons has focus
//   accordion.querySelectorAll('.accordion-trigger').forEach(function (trigger) {
  Array.prototype.slice.call(accordion.querySelectorAll('.accordion-trigger')).forEach(function (trigger) {

    trigger.addEventListener('focus', function (event) {
      accordion.classList.add('focus');
    });

    trigger.addEventListener('blur', function (event) {
      accordion.classList.remove('focus');
    });

  });

  // Minor setup: will set disabled state, via aria-disabled, to an
  // expanded/ active accordion which is not allowed to be toggled close
  if (!allowToggle) {
    // Get the first expanded/ active accordion
    var expanded = accordion.querySelector('[aria-expanded="true"]');

    // If an expanded/ active accordion is found, disable
    if (expanded) {
      expanded.setAttribute('aria-disabled', 'true');
    }
  }

});


/* START - set console styles and console.table fallback */
let consoleStyles;
let consoleStylesError;
let consoleStylesWarning;
if(typeof console.table== 'undefined' || !console.table){
    consoleStyles = '';
    consoleStylesError = '';
    consoleStylesWarning = '';
    console.warn('Not a modern browser');
    console.table = function(data){
        console.log(JSON.stringify(data,null,2));
    }
}else{
    consoleStyles = 'padding: 10px 25px; background-color: #f7f7f7; color: black; font-style: italic; border-left: 10px solid #3778be; font-size: 1.5em;';
    consoleStylesError = 'padding: 10px 25px; background-color: #f7f7f7; color: black; font-style: italic; border-left: 10px solid #a94442; font-size: 1.5em;';
    consoleStylesWarning = 'padding: 10px 25px; background-color: #f7f7f7; color: black; font-style: italic; border-left: 10px solid #ded4a1; font-size: 1.5em;';
}
/* END */


$(document).ready(
    function() {
        var intervalId = setInterval(
            function(){
                var svgready = document.getElementsByTagName('html')[0].getAttribute('class');

                if(svgready){
                    if(svgready.indexOf("fontawesome-i2svg-complete") > -1){
                        // fontawesome has completely loaded, adjust accessibility issues
                        clearInterval(intervalId);
                        $('body .arrow-down-icon').attr('title','show');
                        
                        
                        $('.breadcrumb .breadcrumb-item > svg.svg-inline--fa').attr('role', 'presentation');
                        
                        $('body div[class*="-continue"] > button svg.svg-inline--fa').attr('role','presentation');
                        $('body .btn-submit > svg.svg-inline--fa').attr('role','presentation');
                        $('body .btn-back-link').find('svg.svg-inline--fa').attr('role','presentation');
                        $('body .sq-form-question .select, body .sq-form-question-select .sq-form-question-answer').find('svg.svg-inline--fa').attr('title','select an option');
                        
                        $('body input[autocomplete="offplease"]').attr('autocomplete','off');
                        $('body .accordion-trigger, body .search-icon').find('svg.svg-inline--fa').attr('role', 'presentation');
                        $('body svg.svg-inline--fa').each( function(){
                            $(this).attr('title',$(this).parents().attr('title'));
                        });
                    }
                }
            },
        50);
    }
);
