<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="Consult24Submitter">
    <xsl:choose>
      <xsl:when test="count(UserDetails)=0">
        <!-- Blank Statement -->
      </xsl:when>
      <xsl:otherwise>
        <div class="questionaireShadow">
          &#160;
        </div>
        <xsl:for-each select="./UserDetails">
          <xsl:if test="Do_Details='true'">

            <!-- BEGIN Trade Competition -->
   
               <xsl:if test="Do_Ask_TradeCompetition='true' and MoveHearingToSubmitTab='true'">
              <div id="div_SubmitterDetails_TradeCompetition" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_TradeCompetition='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divHearings" class="questionaireHearings">
                  <span class="">
                    <xsl:value-of select="@lblTradeCompetition" />
                  </span>
                  <xsl:if test="Do_Require_TradeCompetition='true'">
                    <!--<span class="requiredFlag">*</span>-->
                  </xsl:if>
                  <br />
                  <div id="div_SubmitterDetails_TradeAdvantage">
                    
                    
                    <span class="questionaireSubmitterLabel">
                      Could you gain an advantage in trade competition through this submission ?
                    </span>
                    
                    <div class="radio-wrapper">
                        
                   
                      <input type="radio" id="questionaire_TradeAdvantage_item_A" class="questionaireRadio" name="questionaire_TradeAdvantage_item" value="1" onchange="responseChanged();">
                        <xsl:if test="valTradeAdvantage='1'">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>
                       <label for="questionaire_TradeAdvantage_item_A" class="">
                      I could
                    </label>
                    
                    </div>
                    
                     <div class="radio-wrapper">
                         
                    
                      <input type="radio" id="questionaire_TradeAdvantage_item_B" class="questionaireRadio" name="questionaire_TradeAdvantage_item" value="0" onchange="responseChanged();">
                        <xsl:if test="valTradeAdvantage='0'">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>
                      <label for="questionaire_TradeAdvantage_item_B" class="">
                      I could not
                    </label>
                    
                    </div>
                    
                    <br />
                    
                  </div>
                  
                  <div id="div_SubmitterDetails_TradeAffected">
                    
                    
                    <span class="questionaireSubmitterLabel">
                       Are you directly affected by an effect of the subject matter of the submission that :
                    
                    </span>
                    <ul>
                        <li>Adversely affects the environment; and</li>
                        <li>Does not relate to the trade competition or the effects of trade competitions</li>
                    </ul>
                    
                     <div class="radio-wrapper">
                    
                    
                      <input type="radio" id="questionaire_TradeAffected_item_A" class="questionaireRadio" name="questionaire_TradeAffected_item" value="1" onchange="responseChanged();">
                        <xsl:if test="valTradeAffected='1'">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>
                      <label for="questionaire_TradeAffected_item_A" class="">
                      I am
                    </label>
                    
                    
                    </div>
                    
                     <div class="radio-wrapper">
                    
                   
                      <input type="radio" id="questionaire_TradeAffected_item_B" class="questionaireRadio" name="questionaire_TradeAffected_item" value="0" onchange="responseChanged();">
                        <xsl:if test="valTradeAffected='0'">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>
                       <label for="questionaire_TradeAffected_item_B" class="">
                      I am not
                    </label>
                    
                    </div>
                    
                    
                    <br />
                    
                  </div>
                  
                  <div id="FSNote" >
                    <label id="lblFSNotes" runat="server" class="questionaireSubmitterLabel">
                      Note to person making submission:
                    </label>
                    <div id="divTradeNotes"></div>
                    <ul style="padding-left: 20px;">
                      <li class="list_TradeNote" style="list-style: none;">
                        <label class=""> If you are a person who could gain an advantage in trade competition through the submission, your right to make a submission may be limited by clause 6(4) of Part 1 of Schedule 1 of the Resource Management Act 1991</label>
                      </li>
                    </ul>
                  </div>

                </div>
                <div class="clearfix">&#160;</div>
                <div class="advanceButtons">
                  <input type="button" class="ConsultButton ConsultButtonPrevious" onclick="lastQuestion('div_SubmitterDetails_TradeCompetition')" value="Previous" />
                  <input type="button" class="ConsultButton ConsultButtonNext" onclick="nextQuestion('div_SubmitterDetails_TradeCompetition')" value="Next" />
                </div>
              </div>
            </xsl:if>
   
   
            <!-- END Trade Competition -->

            <!-- BEGIN Hearing -->
            
           
             <xsl:if test="Do_Ask_Hearing='true' and MoveHearingToSubmitTab='true'">
              <div id="div_SubmitterDetails_Hearings" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_Hearing='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divHearings" class="questionaireHearings">
                  <span class="questionaireSubmitterLabel">
                    <xsl:value-of select="lblHearing" disable-output-escaping="yes"/>&#160;
                  </span>
                  <xsl:if test="Do_Require_Hearing='true'">
                    <span class=""></span>
                  </xsl:if>
                  <br />
                  <div id="div_SubmitterDetails_WishToBeHeard" onclick="toggleSubmitHearingPart2()">
                    
                    <div class="radio-wrapper">
                  
                      <input type="radio" id="questionaire_HearingYesNo_item_Yes" class="questionaireRadio" name="questionaire_HearingYesNo_item" value="1" onchange="responseChanged();">
                        <xsl:if test="valHearing='true'">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>
                        <label for="questionaire_HearingYesNo_item_Yes" class="">
                      Yes
                    </label>
                    
                    </div>


                    <div class="radio-wrapper">
                   
                      <input type="radio" id="questionaire_HearingYesNo_item_No" class="questionaireRadio" name="questionaire_HearingYesNo_item" value="0" onchange="responseChanged();">
                        <xsl:if test="valHearing='false'">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>
                       <label for="questionaire_HearingYesNo_item_No" class="">
                      I do NOT wish to speak in support of my submission and ask that the following submission be fully considered.
                    </label>
                    </div>
                    
                    
                  </div>
                  <div id="div_submitterDetails_HearingList">
                    <xsl:if test="valHearing='false'">
                      <xsl:attribute name="style">
                        display:none;
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:variable name="hearingContent">
                      <xsl:call-template name="hearingItems">
                        <xsl:with-param name="hearingSelVal">
                          <xsl:value-of select="valSelectedHearing"/>
                        </xsl:with-param>
                      </xsl:call-template>
                    </xsl:variable>
                    <xsl:copy-of select="$hearingContent" />
                    <br />
                    <span class="questionaireSubmitterLabel">
                      <xsl:value-of select="lblHearingNeeds" disable-output-escaping="yes"/>&#160;
                    </span>
                    <textarea name="questionaire_HearingNeeds" rows="2" cols="20" id="questionaire_HearingNeeds" class="qBodyTextArea" onchange="responseChanged();" style="height: 130px;">
                      <xsl:value-of select="valHearingNeeds" disable-output-escaping="yes"/>
                    </textarea>
                  </div>
                </div>
                <div class="clearfix">&#160;</div>
                <div class="advanceButtons">
                  <input type="button" class="ConsultButton ConsultButtonPrevious" onclick="lastQuestion('div_SubmitterDetails_Hearings')" value="Previous" />
                  <input type="button" class="ConsultButton ConsultButtonNext" onclick="nextQuestion('div_SubmitterDetails_Hearings')" value="Next" />
                </div>
              </div>
            </xsl:if>
            
           
           
           
           
            <!-- END Hearing -->
            
            <!-- BEGIN Certain Person -->
            <xsl:if test="Do_Ask_CertainPerson='true' and MoveHearingToSubmitTab='true'">
              <div id="div_SubmitterDetails_CertainPerson" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_Hearing='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divHearings" class="questionaireHearings">
                  <span class="questionaireSubmitterLabel">
                    Person of interest declaration: I am
                  </span>
                  <xsl:if test="Do_Require_CertainPerson='true'">
                    <span class=""></span>
                  </xsl:if>
                  <br /><div class="radio-wrapper">
                  <div id="div_SubmitterDetails_FSCertainPerson" onclick="SubmitterDetails_CertainPerson_Clicked()">
                      <input type="radio" id="questionaire_CertainPersonABC_item_A" class="questionaireRadio" name="questionaire_CertainPersonABC_item" value="1" onchange="submitterChanged();">
                        <xsl:if test="valCertainPerson=1">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>
                    <label for="questionaire_CertainPersonABC_item_A">(a) a person representing a relevant aspect of the public interest, or
                    </label>
                    
                      <input type="radio" id="questionaire_CertainPersonABC_item_B" class="questionaireRadio" name="questionaire_CertainPersonABC_item" value="2" onchange="submitterChanged();">
                        <xsl:if test="valCertainPerson=2">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>
                    <label for="questionaire_CertainPersonABC_item_B">(b) a person who has an interest in the proposal that is greater than the interest the general public has, or
                    </label>
                    

                      <input type="radio" id="questionaire_CertainPersonABC_item_C" class="questionaireRadio" name="questionaire_CertainPersonABC_item" value="3" onchange="submitterChanged();">
                        <xsl:if test="valCertainPerson=3">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>
                    <label for="questionaire_CertainPersonABC_item_C">(c) the local authority for the relevant area.
                    </label>
                  </div>
                  </div>
                  <div id="div_submitterDetails_CertainPersonComment">
                    <br />
                    <span class="questionaireSubmitterLabel">
                      Explain the grounds for saying you come within category (a) or (b) above:
                    </span>
                    <textarea name="questionaire_CertainPersonComment" rows="2" cols="20" id="questionaire_CertainPersonComment" class="qBodyTextArea" onchange="submitterChanged();" style="height: 130px;">
                      <xsl:value-of select="valCertainPersonComment" disable-output-escaping="yes"/>
                    </textarea>
                  </div>
                  <div id="FSNote" >
                    <span class="questionaireSubmitterLabel">
                      Note to person making further submission:
                    </span>
                    <div id="divFSNotes"></div>
                      <ul style="padding-left: 20px;">
                        <li class="list_fsnote">
                          <span class="questionaireSubmitterLabel">A further submission can only support or oppose an original submission listed in the summary. It is not an opportunity to make a fresh submission on matters not raised in the submission.  </span>
                        </li>
                        <li class="list_fsnote">
                          <span class="questionaireSubmitterLabel">A copy of your further submission must be served on the original submitter within 5 working days of making the further submission to the Council </span>
                        </li>
                      </ul>
                    </div>
                  
                </div>
                <div class="clearfix">&#160;</div>
                <div class="advanceButtons">
                  <input type="button" class="ConsultButton ConsultButtonPrevious" onclick="lastQuestion('div_SubmitterDetails_CertainPerson')" value="Previous" />
                  <input type="button" class="ConsultButton ConsultButtonNext" onclick="nextQuestion('div_SubmitterDetails_CertainPerson')" value="Next" />
                </div>
              </div>
            </xsl:if>
            <!-- END Certain Person -->

          </xsl:if>

        </xsl:for-each>

        <div class="clearfix">&#160;</div>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="hearingItems">
    <xsl:param name="hearingSelVal"/>
    <div class="clearfix">&#160;</div>
    <xsl:variable name="findWidth">
      <xsl:if test="count(../HearingsList)>0">
        <xsl:value-of select="100 div count(../HearingsList)"/>
      </xsl:if>
    </xsl:variable>
    <xsl:for-each select="../HearingsList">
      <div>
          <input type="checkbox" id="questionaire_hearing_item_{Hearing_ID}" class="questionaireCheckbox" name="questionaire_hearing_item" value="{Hearing_ID}" onchange="submitterChanged();">
            <xsl:if test="contains($hearingSelVal,concat(',',Hearing_ID,','))">
              <xsl:attribute name="checked">
              </xsl:attribute>
            </xsl:if>
          </input>
        <label for="questionaire_hearing_item_{Hearing_ID}">
          <xsl:value-of select="Hearing_Name" disable-output-escaping="yes"/>
        </label>
      </div>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>