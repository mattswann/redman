<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="Consult24Submitter">
    <xsl:choose>
      <xsl:when test="count(UserDetails)=0">
        <!-- Blank Statement -->
      </xsl:when>
      <xsl:otherwise>
        
        <xsl:for-each select="./UserDetails">
          <xsl:if test="Do_Details='true'">

            <!-- BEGIN Organisation -->
            <xsl:if test="Do_Ask_Organisation='true'">
              <div id="div_SubmitterDetails_Organisation" class="questionaire questionaireSubmitter">
                  <xsl:if test="valOrganisation=''">
                  <xsl:attribute name="style">
                    display:none;
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divOrganisation" class="questionaireOrg" style="float: left; min-width:250px; width:40%; padding-bottom:10px;">
                 <label  class="form-field--label questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblOrganisation" disable-output-escaping="yes"/>&#160;
                  </label>
                  
                  <input name="txtSubmitterOrganisation" id="txtSubmitterOrganisation" class="FormTextBox questionaireTextbox" onchange="submitterChanged();" type="text" value="{valOrganisation}"  />
                </div>
              </div>
            </xsl:if>
            <!-- END Organisation -->

            <!-- BEGIN Submitter Name -->
            <xsl:if test="Do_Ask_SubmitterName='true'">
              <div id="div_SubmitterDetails_Name" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_SubmitterName='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
               
                <div id="divFirstName" class="questionaireName">
                 <label class="form-field--label questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblFirstName" disable-output-escaping="yes"/>&#160;
                  </label>
                  <input name="txtSubmitterFirstName" id="txtSubmitterFirstName" class="FormTextBox questionaireTextbox" placeholder="First Name" onchange="submitterChanged();"  type="text" value="{valFirstName}" style="float:left; width: 100%;  margin-bottom: 10px;"/>
                </div>
                <div id="divLastName" class="questionaireName">
                    <label class="form-field--label questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblLastName" disable-output-escaping="yes"/>&#160;
                  </label>
                  <xsl:if test="Do_Require_SubmitterName='true'">
                    <span class="requiredFlag"></span>
                  </xsl:if>
                  
                  <input name="txtSubmitterLastName" id="txtSubmitterLastName" class="FormTextBox questionaireTextbox" placeholder="Last" onchange="submitterChanged();" type="text" value="{valLastName}"  style="float:left; width: 100%;  margin-bottom: 10px;"/>
                </div>
               
                <xsl:if test="Do_Ask_WithholdDetails='true'">
                  
                  <div id="questionaire_WithholdDetails">
                    
                    <div  class="radio-wrapper">
                         <input type="checkbox" id="questionaire_WithholdDetails" class="questionaireRadio" name="questionaire_WithholdDetails" value="1" onchange="responseChanged();">
                        <xsl:if test="valWithholdDetails='true'">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>
                      
                    <label for="questionaire_WithholdDetails" class="questionaireLabel">
                        <xsl:value-of select="lblWithholdDetails" disable-output-escaping="yes"/>
                    </label>
                    
                    </div>
                    
                  </div>
                </xsl:if>
              </div>
            </xsl:if>
            <!-- END Submitter Name -->

            <!-- BEGIN OnBehalfOf -->
            <xsl:if test="Do_Ask_OnBehalfOf='true'">
              <div id="div_SubmitterDetails_OnBehalfOf" class="questionaire questionaireSubmitter">
                <div class="clearfix">&#160;</div>
                <div id="divOnBehalfOf" class="questionaireOnBehalfOf" style="float: left; min-width:250px; width:40%; padding-bottom:10px;">
                   <label  class="form-field--label questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblOnBehalfOf" disable-output-escaping="yes"/>&#160;
                  </label>
                  
                  <input name="txtSubmitterOnBehalfOf" id="txtSubmitterOnBehalfOf" class="FormTextBox questionaireTextbox" onchange="submitterChanged();" style="width:100%;" type="text" value="{valOnBehalfOf}"  />
                </div>
              </div>
            </xsl:if>
            <!-- END OnBehalfOf -->

            <!-- BEGIN Prefered Contact -->
            <xsl:if test="Do_Ask_PreferedContact='true'">
              <div id="div_SubmitterDetails_PreferedContact" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_PreferedContact='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divPreferedContact" class="questionairePreferedContact">
                  <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblPreferedContact" disable-output-escaping="yes"/>&#160;
                  </span>
                  <xsl:if test="Do_Require_PreferedContact='true'">
                    <span class="requiredFlag"></span>
                  </xsl:if>
                  <xsl:variable name="ContactPrefContent">
                    <xsl:call-template name="ContactPrefListItems">
                      <xsl:with-param name="cpSelVal">
                        <xsl:value-of select="valpreferred_contact"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:variable>
                  <xsl:copy-of select="$ContactPrefContent" />
                </div>
              </div>
            </xsl:if>
            <!-- END Prefered Contact -->
            
            <!-- BEGIN Postal Address -->
            <xsl:if test="Do_Ask_PostalAddress='true'">
              <div id="div_SubmitterDetails_Postal" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_PostalAddress='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="valpreferred_contact!='2'">
                  <xsl:attribute name="style">
                    display:none;
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divEmail" class="questionairePostal" style="float: left; min-width:250px; width:40%; padding-bottom:10px;">
                  <span class="questionaireSubmitterLabel" style="font-weight: bold; padding-top: 10px;">
                    <xsl:value-of select="lblPostal" disable-output-escaping="yes"/>&#160;
                  </span>
                  <xsl:if test="Do_Require_PostalAddress='true'">
                    <span class="requiredFlag">*</span>
                  </xsl:if>
                  <br />
                  <input name="SubmitterPostalAddress" id="SubmitterPostalAddress" class="FormTextBox questionaireTextbox" onchange="submitterChanged();" style="width:100%;" type="text" value="{valPostal}"  placeholder="Type in your address: Eg. 100 Queen St, 1 Willis St"  />
                  <br />
                  <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblPostalSuburb" disable-output-escaping="yes"/>&#160;
                  </span>
                  <br />
                  <input ID="txtSuburb" runat="server" MaxLength="250" Width="width:100%;" value="{valPostalSuburb}" class="FormTextBox questionaireTextbox" onchange='submitterChanged()'></input>

                  <br />
                  <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblPostalCity" disable-output-escaping="yes"/>&#160;
                  </span>
                  <br />
                  <input ID="txtCity" runat="server" MaxLength="250" Width="width:100%;" value="{valPostalCity}" class="FormTextBox questionaireTextbox" onchange='submitterChanged()'></input>
                  <br />
                  <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblPostalCountry" disable-output-escaping="yes"/>&#160;
                  </span>
                  <br />
                  <input ID="txtCountry" runat="server" MaxLength="250" Width="width:100%;" value="{valPostalCountry}" class="FormTextBox questionaireTextbox" onchange='submitterChanged()'></input>
                  <div style="display:none;">
                    <input ID="txtLocation" runat="server" value="{valLocation}"></input>
                  </div>

                  <br />
                  <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblPostCode" disable-output-escaping="yes"/>&#160;
                  </span>
                  <br />
                  <input ID="txtSubmitterPostCode" runat="server" MaxLength="250" Width="width:100%;" value="{valPostCode}" class="FormTextBox questionaireTextbox" onchange='submitterChanged()'></input>
                </div>
              </div>
            </xsl:if>
            <!-- END Postal Address -->

            <!-- BEGIN Email -->
            <xsl:if test="Do_Ask_Email='true'">
              <div id="div_SubmitterDetails_Email" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_Email='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <xsl:if test="valpreferred_contact!='1'">
                  <xsl:attribute name="style">
                    display:none;
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divEmail" class="questionaireEmail" style="float: left; min-width:250px; width:40%; padding-bottom:10px;">
                  <label  class="form-field--label questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblEmail" disable-output-escaping="yes"/>&#160;
                  </label>
                  <xsl:if test="Do_Require_Email='true'">
                    <span class="requiredFlag">*</span>
                  </xsl:if>
                  
                  <input name="txtSubmitterEmail" id="txtSubmitterEmail" class="FormTextBox questionaireTextbox" onchange="submitterChanged();" style="width:100%;" type="text" value="{valEmail}" placeholder="e.g. joe@example.com"  />
                </div>
              </div>
            </xsl:if>
            <!-- END Email -->

            <!-- BEGIN PhoneNumber -->
            <xsl:if test="Do_Ask_PhoneNumber='true'">
              <div id="div_SubmitterDetails_PhoneNumber" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_PhoneNumber='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divPhoneNumber" class="questionairePhoneNumber" style="float: left; min-width:250px; width:40%; padding-bottom:10px;margin-top: 1em !important;">
                  <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblPhone" disable-output-escaping="yes"/>&#160;
                  </span>
                  <xsl:if test="Do_Require_PhoneNumber='true'">
                    <span class="requiredFlag">*</span>
                  </xsl:if>
                  <br />
                  <input name="txtSubmitterDayTimePhoneNumber" id="txtSubmitterDayTimePhoneNumber" class="FormTextBox questionaireTextbox" onchange="submitterChanged();" style="width:100%;" type="text" value="{valPhone}" />
                </div>
              </div>
            </xsl:if>
            <!-- END PhoneNumber -->

            <!-- BEGIN MobileNumber -->
            <xsl:if test="Do_Ask_MobileNumber='true'">
              <div id="div_SubmitterDetails_MobileNumber" class="questionaire questionaireSubmitter">
                <div class="clearfix">&#160;</div>
                <div id="divMobileNumber" class="questionaireMobileNumber" style="float: left; min-width:250px; width:40%; padding-bottom:10px;">
                  <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblMobile" disable-output-escaping="yes"/>&#160;
                  </span>
                  <br />
                  <input name="txtSubmitterMobilePhoneNumber" id="txtSubmitterMobilePhoneNumber" class="FormTextBox questionaireTextbox" onchange="submitterChanged();" style="width:100%;" type="text" value="{valMobile}" />
                </div>
              </div>
            </xsl:if>
            <!-- END MobileNumber -->

            <!-- BEGIN Demographics -->
            <xsl:if test="Do_Ask_Demographics='true'">
              <!-- BEGIN Age -->
              <xsl:if test="Do_Ask_Age='true'">
                <div id="div_SubmitterDetails_Age" class="questionaire questionaireSubmitter">
                  <xsl:if test="Do_Require_Age='true'">
                    <xsl:attribute name="class">
                      <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                    </xsl:attribute>
                  </xsl:if>
                  <div class="clearfix">&#160;</div>
                  <div id="divAge" class="questionaireAge">
                    <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                      Age:<xsl:value-of select="lblAge" disable-output-escaping="yes"/>&#160;
                    </span>
                    <xsl:if test="Do_Require_Age='true'">
                      <span class="requiredFlag">*</span>
                    </xsl:if>
                    <xsl:variable name="AgeDemoContent">
                      <xsl:call-template name="ageDemo">
                        <xsl:with-param name="demoSelVal">
                          <xsl:value-of select="valAge"/>
                        </xsl:with-param>
                      </xsl:call-template>
                    </xsl:variable>
                    <xsl:copy-of select="$AgeDemoContent" />
                  </div>
                </div>
              </xsl:if>
              <!-- END Age -->

              <!-- BEGIN Gender -->
              <xsl:if test="Do_Ask_Gender='true'">
                <div id="div_SubmitterDetails_Gender" class="questionaire questionaireSubmitter">
                  <xsl:if test="Do_Require_Gender='true'">
                    <xsl:attribute name="class">
                      <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                    </xsl:attribute>
                  </xsl:if>
                  <div class="clearfix">&#160;</div>
                  <div id="divGender" class="questionaireGender">
                    <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                      Gender:<xsl:value-of select="lblGender" disable-output-escaping="yes"/>&#160;
                    </span>
                    <xsl:if test="Do_Require_Gender='true'">
                      <span class="requiredFlag">*</span>
                    </xsl:if>
                    <xsl:variable name="GenderDemoContent">
                      <xsl:call-template name="genderDemo">
                        <xsl:with-param name="demoSelVal">
                          <xsl:value-of select="valGender"/>
                        </xsl:with-param>
                      </xsl:call-template>
                    </xsl:variable>
                    <xsl:copy-of select="$GenderDemoContent" />
                  </div>
                </div>
              </xsl:if>
              <!-- END Gender -->

              <!-- BEGIN Ethnicity -->
              <xsl:if test="Do_Ask_Ethnicity='true'">
                <div id="div_SubmitterDetails_Ethnicity" class="questionaire questionaireSubmitter">
                  <xsl:if test="Do_Require_Ethnicity='true'">
                    <xsl:attribute name="class">
                      <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                    </xsl:attribute>
                  </xsl:if>
                  <div class="clearfix">&#160;</div>
                  <div id="divEthnicity" class="questionaireEthnicity">
                    <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                      Ethnicity:<xsl:value-of select="lblEthnicity" disable-output-escaping="yes"/>&#160;
                    </span>
                    <xsl:if test="Do_Require_Ethnicity='true'">
                      <span class="requiredFlag">*</span>
                    </xsl:if>
                    <br />
                    <xsl:variable name="EthnicityDemoContent">
                      <xsl:call-template name="ethnicityDemo">
                        <xsl:with-param name="demoSelVal">
                          <xsl:value-of select="valEthnicity"/>
                        </xsl:with-param>
                      </xsl:call-template>
                    </xsl:variable>
                    <xsl:copy-of select="$EthnicityDemoContent" />
                  </div>
                </div>
              </xsl:if>
              <!-- END Ethnicity -->
            </xsl:if>
            <!-- END Demographics -->

            <!-- BEGIN RatepayerStatus -->
            <xsl:if test="Do_Ask_RatepayerStatus='true'">
              <div id="div_SubmitterDetails_RatepayerStatus" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_RatepayerStatus='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divRatepayerStatus" class="questionaireRatepayerStatus">
                  <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblRatepayerStatus" disable-output-escaping="yes"/>&#160;
                  </span>
                  <xsl:if test="Do_Require_RatepayerStatus='true'">
                    <span class="requiredFlag">*</span>
                  </xsl:if>
                  <br />
                  <xsl:variable name="ratepayerContent">
                    <xsl:call-template name="ratepayerStatusItems">
                      <xsl:with-param name="ratepayerSelVal">
                        <xsl:value-of select="valRatepayerStatus"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:variable>
                  <xsl:copy-of select="$ratepayerContent" />
                </div>
              </div>
            </xsl:if>
            <!-- END RatepayerStatus -->

            <!-- BEGIN PropLocation -->
            <xsl:if test="Do_Ask_PropLocation='true'">
              <div id="div_SubmitterDetails_PropLocation" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_PropLocation='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divPropLocation" class="questionairePropLocation">
                  <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblPropLocation" disable-output-escaping="yes"/>&#160;
                  </span>
                  <xsl:if test="Do_Require_PropLocation='true'">
                    <span class="requiredFlag">*</span>
                  </xsl:if>
                  <br />
                  <xsl:variable name="propLocationContent">
                    <xsl:call-template name="wardItems">
                      <xsl:with-param name="wardSelVal">
                        <xsl:value-of select="valPropLocation"/>
                      </xsl:with-param>
                    </xsl:call-template>
                  </xsl:variable>
                  <xsl:copy-of select="$propLocationContent" />
                </div>
              </div>
            </xsl:if>
            <!-- END PropLocation -->
            
            <!-- BEGIN Trade Competition -->
            <xsl:if test="Do_Ask_TradeCompetition='true' and MoveHearingToSubmitTab!='true'">
              <div id="div_SubmitterDetails_TradeCompetition" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_TradeCompetition='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divHearings" class="questionaireHearings">
                  <span class="questionaireSubmitterLabel">
                    <xsl:value-of select="@lblTradeCompetition" />
                  </span>
                  <xsl:if test="Do_Require_TradeCompetition='true'">
                    <span class=""></span>
                  </xsl:if>
                  <br />
                  <div id="div_SubmitterDetails_TradeAdvantage">
                    <label for="questionaire_TradeAdvantage_item_A" class="questionaireLabel">
                      <input type="radio" id="questionaire_TradeAdvantage_item_A" class="questionaireRadio" name="questionaire_TradeAdvantage_item" value="1">
                        <xsl:if test="valTradeAdvantage='1'">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>I could
                    </label>
                    <br />
                    <label for="questionaire_TradeAdvantage_item_B" class="questionaireLabel">
                      <input type="radio" id="questionaire_TradeAdvantage_item_B" class="questionaireRadio" name="questionaire_TradeAdvantage_item" value="0">
                        <xsl:if test="valTradeAdvantage='0'">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>I could not
                    </label>
                    <br />
                    <span class="questionaireSubmitterLabel">
                      Gain an advantage in trade competition through this submission
                    </span>
                  </div>
                  
                  <div id="div_SubmitterDetails_TradeAffected">
                    <label for="questionaire_TradeAffected_item_A" class="questionaireLabel">
                      <input type="radio" id="questionaire_TradeAffected_item_A" class="questionaireRadio" name="questionaire_TradeAffected_item" value="1">
                        <xsl:if test="valTradeAffected='1'">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>I am
                    </label>
                    <br />
                    <label for="questionaire_TradeAffected_item_B" class="questionaireLabel">
                      <input type="radio" id="questionaire_TradeAffected_item_B" class="questionaireRadio" name="questionaire_TradeAffected_item" value="0">
                        <xsl:if test="valTradeAffected='0'">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>I am not
                    </label>
                    <br />
                    <span class="questionaireSubmitterLabel">
                       directly affected by an effect of the subject matter of the submission that :
                     <br />
                                    a. adversely affects the environment, and
                                        <br />
                                    b. does not relate to the trade competition or the effects of trade competitions.
                    </span>
                  </div>
                  
                  <div id="FSNote" >
                    <span id="lblFSNotes" class="questionaireSubmitterLabel">
                      Note to person making submission:
                    </span>
                    <div id="divTradeNotes">
                        <span class="questionaireSubmitterLabel"> If you are a person who could gain an advantage in trade competition through the submission, your right to make a submission may be limited by clause 6(4) of Part 1 of Schedule 1 of the Resource Management Act 1991 </span>
                      </div>
                  </div>

                </div>
              </div>
            </xsl:if>
            <!-- END Trade Competition -->

            <!-- BEGIN Hearing -->
            <xsl:if test="Do_Ask_Hearing='true'">
              <div id="div_SubmitterDetails_Hearings" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_Hearing='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divHearings" class="questionaireHearings">
                  <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                    <xsl:value-of select="lblHearing" disable-output-escaping="yes"/>&#160;
                  </span>
                  <xsl:if test="Do_Require_Hearing='true'">
                    <span class="requiredFlag">*</span>
                  </xsl:if>
                  <br />
                  <div id="div_SubmitterDetails_WishToBeHeard" onclick="SubmitterDetails_WishToBeHeard_Clicked()">
                    <label for="questionaire_HearingYesNo_item_Yes" class="questionaireLabel">
                      <input type="radio" id="questionaire_HearingYesNo_item_Yes" class="questionaireRadio" name="questionaire_HearingYesNo_item" value="1" onchange="responseChanged();">
                        <xsl:if test="valHearing='true'">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>Yes
                    </label>
                    <br />

                    <label for="questionaire_HearingYesNo_item_No" class="questionaireLabel">
                      <input type="radio" id="questionaire_HearingYesNo_item_No" class="questionaireRadio" name="questionaire_HearingYesNo_item" value="0" onchange="responseChanged();">
                        <xsl:if test="valHearing='false'">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>I do NOT wish to speak in support of my submission and ask that the following submission be fully considered.
                    </label>
                  </div>
                  <div id="div_submitterDetails_HearingList">
                    <xsl:if test="valHearing='false'">
                      <xsl:attribute name="style">
                        display:none;
                      </xsl:attribute>
                    </xsl:if>
                    <xsl:variable name="hearingContent">
                      <xsl:call-template name="hearingItems">
                        <xsl:with-param name="hearingSelVal">
                          <xsl:value-of select="valSelectedHearing"/>
                        </xsl:with-param>
                      </xsl:call-template>
                    </xsl:variable>
                    <xsl:copy-of select="$hearingContent" />
                    <br />
                    <span class="questionaireSubmitterLabel">
                      <xsl:value-of select="lblHearingNeeds" disable-output-escaping="yes"/>&#160;
                    </span>
                    <textarea name="questionaire_HearingNeeds" rows="2" cols="20" id="questionaire_HearingNeeds" class="qBodyTextArea" onchange="responseChanged();" style="height: 130px;">
                      <xsl:value-of select="valHearingNeeds" disable-output-escaping="yes"/>
                    </textarea>
                  </div>
                </div>
              </div>
            </xsl:if>
            <!-- END Hearing -->
            
            
            <!-- BEGIN Certain Person -->
            <xsl:if test="Do_Ask_CertainPerson='true' and MoveHearingToSubmitTab!='true'">
              <div id="div_SubmitterDetails_CertainPerson" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_Hearing='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divHearings" class="questionaireHearings">
                  <span class="questionaireSubmitterLabel">
                    Person of interest declaration: I am
                  </span>
                  <xsl:if test="Do_Require_CertainPerson='true'">
                    <span class=""></span>
                  </xsl:if>
                  <br />
                  <div id="div_SubmitterDetails_FSCertainPerson" onclick="SubmitterDetails_CertainPerson_Clicked()">
                    <label for="questionaire_CertainPersonABC_item_A" class="questionaireLabel">
                      <input type="radio" id="questionaire_CertainPersonABC_item_A" class="questionaireRadio" name="questionaire_CertainPersonABC_item" value="1" onchange="responseChanged();">
                        <xsl:if test="valCertainPerson=1">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>(a) a person representing a relevant aspect of the public interest, or
                    </label>
                    <br />
                    <label for="questionaire_CertainPersonABC_item_B" class="questionaireLabel">
                      <input type="radio" id="questionaire_CertainPersonABC_item_B" class="questionaireRadio" name="questionaire_CertainPersonABC_item" value="2" onchange="responseChanged();">
                        <xsl:if test="valCertainPerson=2">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>(b) a person who has an interest in the proposal that is greater than the interest the general public has, or
                    </label>
                    <br />

                    <label for="questionaire_CertainPersonABC_item_C" class="questionaireLabel">
                      <input type="radio" id="questionaire_CertainPersonABC_item_C" class="questionaireRadio" name="questionaire_CertainPersonABC_item" value="3" onchange="responseChanged();">
                        <xsl:if test="valCertainPerson=3">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>(c) the local authority for the relevant area.
                    </label>
                  </div>
                  <div id="div_submitterDetails_CertainPersonComment">
                    <br />
                    <span class="questionaireSubmitterLabel">
                      Explain the grounds for saying you come within category (a) or (b) above:
                    </span>
                    <textarea name="questionaire_CertainPersonComment" rows="2" cols="20" id="questionaire_CertainPersonComment" class="qBodyTextArea" onchange="responseChanged();" style="height: 130px;">
                      <xsl:value-of select="valCertainPersonComment" disable-output-escaping="yes"/>
                    </textarea>
                  </div>
                  <div id="FSNote" >
                    <span class="questionaireSubmitterLabel">
                      Note to person making further submission:
                    </span>
                    <div id="divFSNotes"></div>
                      <ul style="padding-left: 20px;">
                        <li class="list_fsnote">
                          <span class="questionaireSubmitterLabel">A further submission can only support or oppose an original submission listed in the summary. It is not an opportunity to make a fresh submission on matters not raised in the submission.  </span>
                        </li>
                        <li class="list_fsnote">
                          <span class="questionaireSubmitterLabel">A copy of your further submission must be served on the original submitter within 5 working days of making the further submission to the Council </span>
                        </li>
                      </ul>
                    </div>
                  
                </div>
              </div>
            </xsl:if>
            <!-- END Certain Person -->

            <!-- BEGIN Correspondence (Agent) -->
            <xsl:if test="Do_Ask_Correspondence='true'">

              <div id="div_SubmitterDetails_Correspondence" class="questionaire questionaireSubmitter">
                <xsl:if test="Do_Require_Correspondence='true'">
                  <xsl:attribute name="class">
                    <xsl:value-of select="@class" />questionaire questionaireSubmitter questionaireRequired
                  </xsl:attribute>
                </xsl:if>
                <div class="clearfix">&#160;</div>
                <div id="divCorrespondence" class="questionaireCorrespondence">
                  <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                    Correspondence to:
                  </span>
                  <xsl:if test="Do_Require_Correspondence='true'">
                    <span class="requiredFlag">*</span>
                  </xsl:if>
                  <br />

                  <div id="questionaire_CorrespondenceTo_item_1" style="float: left; min-width:200px; width:33%; padding-bottom:10px;">
                    <label for="questionaire_CorrespondenceTo_item_1" class="questionaireLabel">
                      <input type="checkbox" id="corrSubmitter" class="questionaireCheckbox" name="questionaire_CorrespondenceTo_item" value="1" onchange="correspondenceChanged(1);">
                        <xsl:if test="valCorrespondenceTo=1">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>
                      Submitter
                    </label>
                  </div>

                  <div id="questionaire_CorrespondenceTo_item_2" style="float: left; min-width:200px; width:33%; padding-bottom:10px;">
                    <label for="corrAgent" class="questionaireLabel" style="font-weight: bold;">
                      <input type="checkbox" id="corrAgent" class="questionaireCheckbox" name="questionaire_CorrespondenceTo_item" value="2" onchange="correspondenceChanged(2);">
                        <xsl:if test="valCorrespondenceTo=2">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>
                      Agent
                    </label>
                  </div>

                  <div id="questionaire_CorrespondenceTo_item_3" style="float: left; min-width:200px; width:33%; padding-bottom:10px;">
                    <label for="corrBoth" class="questionaireLabel" style="font-weight: bold;">
                      <input type="checkbox" id="corrBoth" class="questionaireCheckbox" name="questionaire_CorrespondenceTo_item" value="3" onchange="correspondenceChanged(3);">
                        <xsl:if test="valCorrespondenceTo=3">
                          <xsl:attribute name="checked">
                          </xsl:attribute>
                        </xsl:if>
                      </input>
                      Both
                    </label>
                  </div>
                
                
                
                </div>
                <div class="clearfix">&#160;</div>
                <div class="advanceButtons">
                  <input type="button" class="ConsultButton ConsultButtonPrevious" onclick="lastQuestion('div_SubmitterDetails_Correspondence')" value="Previous" />
                  <input type="button" class="ConsultButton ConsultButtonNext" onclick="nextQuestion('div_SubmitterDetails_Correspondence')" value="Next" />
                </div>
              </div>




              <div id="pnlMainAgentDetails" style="display: none;">
                <div class="questionaire" id="div_Agent_Information">
                  <div class="legend questionaireAgentDescLabel">
                    <xsl:value-of select="lblAgentDesc" disable-output-escaping="yes"/>&#160;
                  </div>

                  <div id="divAgentName" class="questionaireAgentName" style="float: left; min-width:250px; width:40%; padding-bottom:10px;">
                    <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                      <xsl:value-of select="lblAgentName" disable-output-escaping="yes"/>&#160;
                    </span>
                    <br />
                    <input name="txtAgentName" id="txtAgentName" class="FormTextBox questionaireTextbox" onchange="submitterChanged();" style="width:100%;" type="text" value="{valAgentName}"  />
                  </div>
                  <div class="clearfix">&#160;</div>

                  <div id="divAgentOrganisation" class="questionaireAgentOrganisation" style="float: left; min-width:250px; width:40%; padding-bottom:10px;">
                    <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                      <xsl:value-of select="lblAgentOrganisation" disable-output-escaping="yes"/>&#160;
                    </span>
                    <br />
                    <input name="txtAgentOrganisation" id="txtAgentOrganisation" class="FormTextBox questionaireTextbox" onchange="submitterChanged();" style="width:100%;" type="text" value="{valAgentOrg}"  />
                  </div>
                  <div class="clearfix">&#160;</div>

                  <div id="divAgentPostal" class="questionaireAgentPostal" style="float: left; min-width:250px; width:40%; padding-bottom:10px;">
                    <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                      <xsl:value-of select="lblAgentPostal" disable-output-escaping="yes"/>&#160;
                    </span>
                    <br />
                    <input name="txtAgentPostalAddress" id="txtAgentPostalAddress" class="FormTextBox questionaireTextbox" onchange="submitterChanged();" style="width:100%;" type="text" value="{valAgentPostal}"  />
                  </div>
                  <div class="clearfix">&#160;</div>

                  <div id="divAgentPhone" class="questionaireAgentPhone" style="float: left; min-width:250px; width:40%; padding-bottom:10px;">
                    <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                      <xsl:value-of select="lblAgentPhone" disable-output-escaping="yes"/>&#160;
                    </span>
                    <br />
                    <input name="AgentPhone" id="AgentPhone" class="FormTextBox questionaireTextbox" onchange="submitterChanged();" style="width:100%;" type="text" value="{valAgentPhone}"  />
                  </div>
                  <div class="clearfix">&#160;</div>

                  <div id="divAgentMobile" class="questionaireAgentMobile" style="float: left; min-width:250px; width:40%; padding-bottom:10px;">
                    <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                      <xsl:value-of select="lblAgentMobile" disable-output-escaping="yes"/>&#160;
                    </span>
                    <br />
                    <input name="AgentMobile" id="AgentMobile" class="FormTextBox questionaireTextbox" onchange="submitterChanged();" style="width:100%;" type="text" value="{valAgentMobile}"  />
                  </div>
                  <div class="clearfix">&#160;</div>

                  <div id="divAgentEmail" class="questionaireAgentEmail" style="float: left; min-width:250px; width:40%; padding-bottom:10px;">
                    <span class="questionaireSubmitterLabel" style="font-weight: bold;">
                      <xsl:value-of select="lblAgentEmail" disable-output-escaping="yes"/>&#160;
                    </span>
                    <br />
                    <input name="txtAgentEmail" id="txtAgentEmail" class="FormTextBox questionaireTextbox" onchange="submitterChanged();" style="width:100%;" type="text" value="{valAgentEmail}" placeholder="e.g. joe@example.com" />
                  </div>
                </div>
              </div>

            </xsl:if>
            <!-- END Correspondence -->

          </xsl:if>

        </xsl:for-each>

        <div class="clearfix">&#160;</div>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>


  <xsl:template name="ageDemo">
    <xsl:param name="demoSelVal"/>
    <div class="clearfix">&#160;</div>
    <xsl:variable name="findWidth">
      <xsl:if test="count(../AgeList[demographicEnabled='true'])>0">
        <xsl:value-of select="100 div count(../AgeList[demographicEnabled='true'])"/>
      </xsl:if>
    </xsl:variable>
    <xsl:for-each select="../AgeList[demographicEnabled='true']">
      
      <div id="questionaire_demo_age_item_{demographicID}">
          
          <div  class="radio-wrapper">
          <input type="radio" id="questionaire_demo_age_item_{demographicID}" class="questionaireRadio" name="questionaire_demo_age_item" value="{demographicID}" onchange="responseChanged();">
            <xsl:if test="demographicID=$demoSelVal">
              <xsl:attribute name="checked">
              </xsl:attribute>
            </xsl:if>
          </input>
            <label for="questionaire_demo_age_item_{demographicID}" class="questionaireLabel">
          <xsl:value-of select="demographicDescription" disable-output-escaping="yes"/>
        </label>
        </div>
      </div>
      
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="genderDemo">
    <xsl:param name="demoSelVal"/>
    <div class="clearfix">&#160;</div>
    <xsl:variable name="findWidth">
      <xsl:if test="count(../GenderList[demographicEnabled='true'])>0">
        <xsl:value-of select="100 div count(../GenderList[demographicEnabled='true'])"/>
      </xsl:if>
    </xsl:variable>
    <xsl:for-each select="../GenderList[demographicEnabled='true']">
      
      <div id="questionaire_demo_gender_item_{demographicID}">
         <div class="radio-wrapper">
             <input type="radio" id="questionaire_demo_gender_item_{demographicID}" class="questionaireRadio" name="questionaire_demo_gender_item" value="{demographicID}" onchange="responseChanged();">
            <xsl:if test="demographicID=$demoSelVal">
              <xsl:attribute name="checked">
              </xsl:attribute>
            </xsl:if>
          </input>
        <label for="questionaire_demo_gender_item_{demographicID}" class="questionaireLabel">
          <xsl:value-of select="demographicDescription" disable-output-escaping="yes"/>
        </label>
        </div>
      </div>
      
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="ethnicityDemo">
    <xsl:param name="demoSelVal"/>
    <div class="clearfix">&#160;</div>
    <xsl:variable name="findWidth">
      <xsl:if test="count(../EthnicityList[demographicEnabled='true'])>0">
        <xsl:value-of select="100 div count(../EthnicityList[demographicEnabled='true'])"/>
      </xsl:if>
    </xsl:variable>
    <xsl:for-each select="../EthnicityList[demographicEnabled='true']">
      
      <div id="questionaire_demo_ethnicity_item_{demographicID}">
          
      <div  class="radio-wrapper">
       <input type="checkbox" id="questionaire_demo_ethnicity_item_{demographicID}" class="questionaireRadio" name="questionaire_demo_ethnicity_item" value="{demographicID}" onchange="responseChanged();">
            <xsl:if test="contains($demoSelVal,concat(',',demographicID,','))">
              <xsl:attribute name="checked">
              </xsl:attribute>
            </xsl:if>
          </input>
        <label for="questionaire_demo_ethnicity_item_{demographicID}" class="questionaireLabel">
         
          <xsl:value-of select="demographicDescription" disable-output-escaping="yes"/>
        </label>
      </div>
      </div>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="ContactPrefListItems">
    <xsl:param name="cpSelVal"/>
    <div class="clearfix">&#160;</div>
    <xsl:variable name="findWidth">
      <xsl:if test="count(../ContactPrefList)>0">
        <xsl:value-of select="100 div count(../ContactPrefList)"/>
      </xsl:if>
    </xsl:variable>
    <xsl:for-each select="../ContactPrefList">
      
      <div>
        
         <div  class="radio-wrapper">
        
         <input type="radio" id="questionaire_contact_preference_item_{contactPrefID}" class="questionaireRadio" name="questionaire_contact_preference_item" value="{contactPrefID}" onchange="responseChanged();">
            <xsl:if test="contactPrefID=$cpSelVal">
              <xsl:attribute name="checked">
              </xsl:attribute>
            </xsl:if>
          </input>
        
        <label for="questionaire_contact_preference_item_{contactPrefID}" class="questionaireLabel"  style="color: #666 !important; width: 100% !important; float: left; margin-top: -10px;">
         
          <xsl:value-of select="contactPrefDescription" disable-output-escaping="yes"/>
        </label>
        
        </div>
      </div>
      
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="ratepayerStatusItems">
    <xsl:param name="ratepayerSelVal"/>
    <div class="clearfix">&#160;</div>
    <xsl:variable name="findWidth">
      <xsl:if test="count(../RatePayerStatusOptions)>0">
        <xsl:value-of select="100 div count(../RatePayerStatusOptions)"/>
      </xsl:if>
    </xsl:variable>
    <xsl:for-each select="../RatePayerStatusOptions">
      
      <div id="questionaire_ratepayer_item_{RatePayer_ID}">
        <div  class="radio-wrapper">
            <input type="radio" id="questionaire_ratepayer_item_{RatePayer_ID}" class="questionaireRadio" name="questionaire_ratepayer_item" value="{RatePayer_ID}" onchange="responseChanged();">
            <xsl:if test="RatePayer_ID=$ratepayerSelVal">
              <xsl:attribute name="checked">
              </xsl:attribute>
            </xsl:if>
          </input>
        <label for="questionaire_demo_ethnicity_item_{RatePayer_ID}" class="questionaireLabel">
          <xsl:value-of select="RatePayer_Name" disable-output-escaping="yes"/>
        </label>
        </div>
      </div>
      
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="wardItems">
    <xsl:param name="wardSelVal"/>
    <div class="clearfix">&#160;</div>
    <xsl:variable name="findWidth">
      <xsl:if test="count(../WardList)>0">
        <xsl:value-of select="100 div count(../WardList)"/>
      </xsl:if>
    </xsl:variable>
    <xsl:for-each select="../WardList">
      
      <div id="questionaire_ward_item_{Ward_ID}">
      <div  class="radio-wrapper">  
       <input type="checkbox" id="questionaire_ward_item_{Ward_ID}" class="questionaireRadio" name="questionaire_ward_item" value="{Ward_ID}" onchange="responseChanged();">
            <xsl:if test="contains($wardSelVal,concat(',',Ward_ID,','))">
              <xsl:attribute name="checked">
              </xsl:attribute>
            </xsl:if>
          </input>
        <label for="questionaire_ward_item_{Ward_ID}" class="questionaireLabel">
          <xsl:value-of select="Ward_Name" disable-output-escaping="yes"/>
        </label>
      </div>
      </div>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="hearingItems">
    <xsl:param name="hearingSelVal"/>
    <div class="clearfix">&#160;</div>
    <xsl:variable name="findWidth">
      <xsl:if test="count(../HearingsList)>0">
        <xsl:value-of select="100 div count(../HearingsList)"/>
      </xsl:if>
    </xsl:variable>
    <xsl:for-each select="../HearingsList">
      <div id="questionaire_hearing_item_{Hearing_ID}" style="float: left; min-width:200px; width:{$findWidth}%; padding-bottom:10px;">
        <label for="questionaire_hearing_item_{Hearing_ID}" class="questionaireLabel">
          <input type="checkbox" id="questionaire_hearing_item_{Hearing_ID}" class="questionaireCheckbox" name="questionaire_hearing_item" value="{Hearing_ID}" onchange="responseChanged();">
            <xsl:if test="contains($hearingSelVal,concat(',',Hearing_ID,','))">
              <xsl:attribute name="checked">
              </xsl:attribute>
            </xsl:if>
          </input>
          <xsl:value-of select="Hearing_Name" disable-output-escaping="yes"/>
        </label>
      </div>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>