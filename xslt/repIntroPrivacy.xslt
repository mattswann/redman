<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="Consult24Submitter">
    <xsl:choose>
      <xsl:when test="count(UserDetails)=0">
        <!-- Blank Statement -->
      </xsl:when>
      <xsl:otherwise>
        <xsl:for-each select="./UserDetails">
          <xsl:if test="Do_Print_Introduction='true'">
            <xsl:if test="Do_Show_Introduction='true'">
              <div class="questionaire" id="div_Introduction">
                <div class="legend questionaireIntroLabel">
                  Introductions
                </div>
                <div class="questionaireIntro">
                  <p><xsl:value-of select="IntroductionText" disable-output-escaping="yes"/>&#160;</p>
                </div>
              </div>
            </xsl:if>
          </xsl:if>
          <xsl:if test="Do_Print_Privacy='true'">
            <xsl:if test="Do_Show_Privacy='true'">
              <div class="questionaire" id="div_Privacy_Statement">
                <div class="legend questionairePrivacyLabel">
                  Privacy Statement
                </div>
                <div class="questionairePrivacy">
                  <p>
                    <xsl:value-of select="PrivacyStatement" disable-output-escaping="yes"/>&#160;
                  </p>
                </div>
              </div>
            </xsl:if>
          </xsl:if>
        </xsl:for-each>

        <div class="clearfix">&#160;</div>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>