<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="hideIndex"/>
  <xsl:param name="DoPrintQuestion"/>
  <xsl:param name="DoPrintQuestionNum"/>
  <xsl:template match="NewDataSet">
    <xsl:choose>
      <xsl:when test="count(ResponseParent)=0">
        <!-- Blank Statement -->
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="$hideIndex='False'">
          <div id="div_ResponseIndex" class="questionaire questionaireIndex" style="padding-bottom: 20px; padding-top: 20px;">
            <span style="font-weight:bold; text-decoration: underline;">Index</span> (Please click on the link below to jump to that question)<br/>
            <xsl:for-each select="./ResponseParent">
              &#8226; <a href="#" onclick="$('#{RMID}')[0].scrollIntoView(); return false;">
                <xsl:value-of select="ResponseFieldName" disable-output-escaping="yes"/>
              </a>
              <br />
            </xsl:for-each>
          </div>
        </xsl:if>

        <xsl:for-each select="./ResponseParent">
          <xsl:variable name="parentID">
            <xsl:value-of select="RMID" disable-output-escaping="yes"/>
          </xsl:variable>
          <xsl:variable name="responseType">
            <xsl:value-of select="ResponseFieldType" disable-output-escaping="yes"/>
          </xsl:variable>
          <xsl:if test="position()>1">
            <hr/>
          </xsl:if>
          <div id="div_ResponseAnswer_{RMID}" class="questionaire questionaireItem">
            <xsl:if test="ResponseFieldIsMandatory='True'">
              <xsl:attribute name="class">
                <xsl:value-of select="@class" />questionaire questionaireRequired
              </xsl:attribute>
            </xsl:if>
            <p>
              <xsl:if test="$DoPrintQuestion='True'">
              <xsl:value-of select="ResponseFieldDetails" disable-output-escaping="yes"/>
              </xsl:if>
              <a id="{RMID}" name="{RMID}">&#160;</a>
              <br/>

              <xsl:if test="$DoPrintQuestionNum='True'">
                <span class="questionaire_numbers">
                  <xsl:value-of select="ResponseFieldNumber" disable-output-escaping="yes"/>&#160;
                </span>
              </xsl:if>
              <xsl:if test="$DoPrintQuestion='True'">
                <span class="questionaire_text">
                  <xsl:value-of select="ResponseFieldQuestion" disable-output-escaping="yes"/>
                </span>
              </xsl:if>
              <xsl:if test="$DoPrintQuestion='False'">
                <span class="questionaire_text">
                  <xsl:value-of select="ResponseFieldName" disable-output-escaping="yes"/>
                </span>
              </xsl:if>
              <xsl:if test="ResponseFieldIsMandatory='True'">
                <span class="requiredFlag">*</span>
              </xsl:if>
            </p>
            <xsl:variable name="childContent">
              <xsl:choose>
                <xsl:when test="$responseType='Decision Requested'">
                  <xsl:call-template name="childDecisionRequested">
                    <xsl:with-param name="ParentID">
                      <xsl:value-of select="RMID"/>
                    </xsl:with-param>
                    <xsl:with-param name="qPosition">
                      <xsl:value-of select="position()"/>
                    </xsl:with-param>
                  </xsl:call-template>
                </xsl:when>
                <xsl:when test="$responseType='Comments'">
                  <xsl:call-template name="childComments">
                    <xsl:with-param name="ParentID">
                      <xsl:value-of select="RMID"/>
                    </xsl:with-param>
                    <xsl:with-param name="qPosition">
                      <xsl:value-of select="position()"/>
                    </xsl:with-param>
                  </xsl:call-template>
                </xsl:when>
                <xsl:when test="$responseType='Radio Buttons'">
                  <xsl:call-template name="childRadioButtons">
                    <xsl:with-param name="ParentID">
                      <xsl:value-of select="RMID"/>
                    </xsl:with-param>
                    <xsl:with-param name="qPosition">
                      <xsl:value-of select="position()"/>
                    </xsl:with-param>
                  </xsl:call-template>
                </xsl:when>
                <xsl:when test="$responseType='Multiple Choice'">
                  <xsl:call-template name="childMultipleChoice">
                    <xsl:with-param name="ParentID">
                      <xsl:value-of select="RMID"/>
                    </xsl:with-param>
                    <xsl:with-param name="qPosition">
                      <xsl:value-of select="position()"/>
                    </xsl:with-param>
                  </xsl:call-template>
                </xsl:when>
                <xsl:when test="$responseType='Rating Scale'">
                  <xsl:call-template name="childRatingScale">
                    <xsl:with-param name="ParentID" >
                      <xsl:value-of select="RMID" />
                    </xsl:with-param>
                    <xsl:with-param name="qPosition">
                      <xsl:value-of select="position()"/>
                    </xsl:with-param>
                  </xsl:call-template>
                </xsl:when>
                <xsl:when test="$responseType='Matrix of Choices'">
                  <xsl:call-template name="childMatrixDivs">
                    <xsl:with-param name="ParentID">
                      <xsl:value-of select="RMID"/>
                    </xsl:with-param>
                    <xsl:with-param name="qPosition">
                      <xsl:value-of select="position()"/>
                    </xsl:with-param>
                  </xsl:call-template>
                </xsl:when>
                <xsl:otherwise></xsl:otherwise>
              </xsl:choose>
            </xsl:variable>

            <xsl:copy-of select="$childContent" />


          </div>
        </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="childDecisionRequested">
    <xsl:param name="ParentID"/>
    <xsl:param name="qPosition"/>
    <div class="questionaireDiv questionaireDnR questionaire_{$qPosition}" id="questionaire_q_Decision_{RMID}">
      <xsl:for-each select="../ResponseChild[RMID=$ParentID]">
        <label class="questionaireLabel">
          <xsl:choose>
            <xsl:when test="ResponseFieldLabel='' and position()=1">Decision Requested</xsl:when>
            <xsl:when test="ResponseFieldLabel='' and position()>1">Reason for Decision Requested</xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="ResponseFieldLabel" disable-output-escaping="yes"/>
            </xsl:otherwise>
          </xsl:choose>
        </label>
        <div name="questionaire_txt_Decision_{RCID}_{RMID}" rows="2" cols="20" id="questionaire_txt_Comments_{RCID}_{RMID}" class="qBodyTextArea ConsultEditor" onchange="responseChanged();" style="height: 100%;">
          <xsl:value-of select="ChildAnswer" disable-output-escaping="yes"/>
        </div>
      </xsl:for-each>
    </div>
  </xsl:template>

  <xsl:template name="childComments">
    <xsl:param name="ParentID"/>
    <xsl:param name="qPosition"/>
    <div class="questionaireDiv questionaireComments questionaire_{$qPosition}" id="questionaire_q_Comment_{RMID}">
      <xsl:for-each select="../ResponseChild[RMID=$ParentID]">
        <label class="questionaireLabel">
          <xsl:value-of select="ResponseFieldLabel" disable-output-escaping="yes"/>
        </label>
        <div name="questionaire_txt_Comments_{RCID}_{RMID}" rows="2" cols="20" id="questionaire_txt_Comments_{RCID}_{RMID}" class="qBodyTextArea ConsultEditor" onchange="responseChanged();" style="height: 100%;">
          <xsl:value-of select="ChildAnswer" disable-output-escaping="yes"/>
        </div>
      </xsl:for-each>
    </div>

  </xsl:template>

  <xsl:template name="childRadioButtons">
    <xsl:param name="ParentID"/>
    <xsl:param name="qPosition"/>
    <div class="questionaireDiv questionaireRadio questionaire_{$qPosition}" id="questionaire_q_Radio_{RMID}">
      <xsl:for-each select="../ResponseChild[RMID=$ParentID]">

        <div class="radio-wrapper">
             <input type="radio" id="questionaire_rdoList_SingleChoice_{RCID}_{RMID}" name="questionaire_rdoList_SingleChoice_{RMID}"  value="{RCID}"  onchange="responseChanged();">
            <xsl:if test="ChildAnswer=1">
              <xsl:attribute name="checked">
              </xsl:attribute>
            </xsl:if>
          </input>
        <label for="questionaire_rdoList_SingleChoice_{RCID}_{RMID}" class="questionaireLabel">
          <xsl:value-of select="ResponseFieldLabel" disable-output-escaping="yes"/>
        </label>
        </div>
        
      </xsl:for-each>
      <xsl:for-each select="../ResponseChild[RMID=$ParentID]">
        <xsl:if test="ResponseFieldAddTextAreaYesNo='true' and position()=1">
          <br/>
          <label class="questionaireLabel">
            <xsl:value-of select="ResponseFieldAddTextAreaLabel" disable-output-escaping="yes"/>
          </label>
          <div name="questionaire_txt_RadioOther_{RCID}_{RMID}" rows="2" cols="20" id="questionaire_txt_RadioOther_{RCID}_{RMID}" class="qBodyTextArea ConsultEditor" onchange="responseChanged();" style="height: 100%;">
            <xsl:value-of select="ChildAnswerTextArea" disable-output-escaping="yes"/>
          </div>
        </xsl:if>
      </xsl:for-each>
    </div>
  </xsl:template>

  <xsl:template name="childMultipleChoice">
    <xsl:param name="ParentID"/>
    <xsl:param name="qPosition"/>
    <div class="questionaireDiv questionaireMultipleChoice questionaire_{$qPosition}" id="questionaire_q_MultiChoice_{RMID}">
      <xsl:for-each select="../ResponseChild[RMID=$ParentID]">
       
        <label for="questionaire_rdoList_SingleChoice_{RCID}_{RMID}" class="checkbox-wrapper m-b-2 m-t-1 questionaireLabel" style="float: left; width: 100%; margin-bottom: 10px;">
          <input type="checkbox" id="questionaire_chkList_MultiChoice_{RCID}_{RMID}" name="questionaire_chkList_MultiChoice_{RMID}" value="{RCID}" onchange="responseChanged();">
            <xsl:if test="ChildAnswer=1">
              <xsl:attribute name="checked">
              </xsl:attribute>
            </xsl:if>
          </input>
          <span><xsl:value-of select="ResponseFieldLabel" disable-output-escaping="yes"/></span>
        </label>
        
      </xsl:for-each>
      <xsl:for-each select="../ResponseChild[RMID=$ParentID]">
        <xsl:if test="ResponseFieldAddTextAreaYesNo='true' and position()=1">
          <br/>
          <label class="questionaireLabel">
            <xsl:value-of select="ResponseFieldAddTextAreaLabel" disable-output-escaping="yes"/>
          </label>
          <div name="questionaire_txt_CheckBoxOther_{RCID}_{RMID}" rows="2" cols="20" id="questionaire_txt_CheckBoxOther_{RCID}_{RMID}" class="qBodyTextArea ConsultEditor" onchange="responseChanged();" style="height: 100%;">
            <xsl:value-of select="ChildAnswerTextArea" disable-output-escaping="yes"/>
          </div>
        </xsl:if>
      </xsl:for-each>
    </div>
  </xsl:template>

  <xsl:template name="childRatingScale">
    <xsl:param name="ParentID"/>
    <xsl:param name="qPosition"/>
    <div class="questionaireDiv questionaireRating questionaire_{$qPosition}" id="questionaire_q_Rating_{RMID}">
      <table>
        <tr class="questionaireRatingLabels">
          <xsl:for-each select="../ResponseChild[RMID=$ParentID]">
            <td>
              <label for="questionaire_rdoList_Rating_{RCID}_{RMID}" class="questionaireLabel"></label>
              <xsl:value-of select="ResponseFieldLabel" disable-output-escaping="yes"/>
            </td>
          </xsl:for-each>
        </tr>
        <tr class="questionaireRatingRadios">
          <xsl:for-each select="../ResponseChild[RMID=$ParentID]">
            <td>
              <input type="radio" id="questionaire_rdoList_Rating_{RCID}_{RMID}" name="questionaire_rdoList_Rating_{RMID}" value="{RCID}" onchange="responseChanged();">
                <xsl:if test="ChildAnswer=1">
                  <xsl:attribute name="checked">
                  </xsl:attribute>
                </xsl:if>
              </input>
            </td>
          </xsl:for-each>
        </tr>
      </table>
      <xsl:for-each select="../ResponseChild[RMID=$ParentID]">
        <xsl:if test="ResponseFieldAddTextAreaYesNo='true' and position()=1">
          <br/>
          <label class="questionaireLabel">
            <xsl:value-of select="ResponseFieldAddTextAreaLabel" disable-output-escaping="yes"/>
          </label>
          <div name="questionaire_txt_RatingOther_{RCID}_{RMID}" rows="2" cols="20" id="questionaire_txt_RatingOther_{RCID}_{RMID}" class="qBodyTextArea ConsultEditor" onchange="responseChanged();" style="height: 100%;">
            <xsl:value-of select="ChildAnswerTextArea" disable-output-escaping="yes"/>
          </div>
        </xsl:if>
      </xsl:for-each>
    </div>
  </xsl:template>

  <xsl:template name="childMatrix">
    <xsl:param name="ParentID"/>
    <xsl:param name="qPosition"/>
    <div class="questionaireDiv questionaireMatrix questionaire_{$qPosition}" id="questionaire_q_Matrix_{RMID}">
      <table>
        <tr>
          <xsl:for-each select="../ResponseForMatrix[RMID=$ParentID and (ResponseFieldColName!='' and ResponseFieldColName!=' ')]">
            <xsl:if test="ISCellA1Added='false' and position()=1">
              <td></td>
            </xsl:if>
            <td>
              <xsl:value-of select="ResponseFieldColName" disable-output-escaping="yes"/>
            </td>
          </xsl:for-each>
        </tr>

        <xsl:for-each select="../ResponseForMatrix[RMID=$ParentID and ResponseFieldRowName!='']">
          <xsl:variable name="matrixRowID">
            <xsl:value-of select="RMatrixID" disable-output-escaping="yes"/>
          </xsl:variable>
          <tr>
            <td>
              <xsl:value-of select="ResponseFieldRowName" disable-output-escaping="yes"/>
            </td>
            <xsl:for-each select="../ResponseForMatrix[RMID=$ParentID and (ResponseFieldColName!='' and ResponseFieldColName!=' ')]">
              <xsl:variable name="matrixColumnID">
                <xsl:value-of select="RMatrixID" disable-output-escaping="yes"/>
              </xsl:variable>
              <xsl:variable name="matrixSelected">
                <xsl:call-template name="childMatrixAnswers">
                  <xsl:with-param name="ParentID">
                    <xsl:value-of select="RMID"/>
                  </xsl:with-param>
                  <xsl:with-param name="matrixRowID">
                    <xsl:value-of select="$matrixRowID"/>
                  </xsl:with-param>
                  <xsl:with-param name="matrixColumnID">
                    <xsl:value-of select="$matrixColumnID"/>
                  </xsl:with-param>
                </xsl:call-template>
              </xsl:variable>
              <xsl:if test="(ISCellA1Added='true' and position()>1) or (ISCellA1Added='false')">
                <td>
                  <xsl:if test="ISSingleChoiceEnabled='false'">
                    <input type="radio" id="questionaire_matrix_ctrl_{RMID}_{$matrixRowID}_{RMatrixID}" name="questionaire_matrix_group_{$matrixRowID}" value="{$matrixRowID}_{RMatrixID}"  onchange="responseChanged();">
                      <xsl:if test="$matrixSelected=1">
                        <xsl:attribute name="checked">
                        </xsl:attribute>
                      </xsl:if>

                    </input>

                  </xsl:if>
                  <xsl:if test="ISSingleChoiceEnabled='true'">
                    <input type="checkbox" id="questionaire_matrix_ctrl_{RMID}_{$matrixRowID}_{RMatrixID}" name="questionaire_matrix_group_{$matrixRowID}" value="{$matrixRowID}_{RMatrixID}" onchange="responseChanged();">
                      <xsl:if test="$matrixSelected=1">
                        <xsl:attribute name="checked">
                        </xsl:attribute>
                      </xsl:if>

                    </input>
                  </xsl:if>
                </td>
              </xsl:if>
            </xsl:for-each>
          </tr>
        </xsl:for-each>
      </table>
      <xsl:for-each select="../ResponseForMatrix[RMID=$ParentID]">
        <xsl:if test="ISTextAreaAdded='true' and position()=1">
          <xsl:variable name="matrixComment">
            <xsl:call-template name="childMatrixAnswersComments">
              <xsl:with-param name="ParentID">
                <xsl:value-of select="RMID"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:variable>
          <br/>
          <label class="questionaireLabel">
            <xsl:value-of select="ResponseFieldAddTextAreaLabel" disable-output-escaping="yes"/>
          </label>
          <div name="questionaire_txt_MatrixOther_{RMatrixID}_{RMID}" rows="2" cols="20" id="questionaire_txt_MatrixOther_{RMatrixID}_{RMID}" class="qBodyTextArea ConsultEditor" onchange="responseChanged();" style="height: 100%;">
            <xsl:copy-of select="$matrixComment" />&#160;
          </div>
        </xsl:if>
      </xsl:for-each>
    </div>
  </xsl:template>

  <xsl:template name="childMatrixAnswers">
    <xsl:param name="ParentID"/>
    <xsl:param name="matrixRowID"/>
    <xsl:param name="matrixColumnID"/>
    <xsl:for-each select="../MatrixAnswers[ResponseField_Master_ID=$ParentID and ResponseField_Child_ID=$matrixRowID and ResponseField_Child2_ID=$matrixColumnID]">
      <xsl:value-of select="Answer" disable-output-escaping="yes"/>
    </xsl:for-each>
  </xsl:template>


  <xsl:template name="childMatrixDivs">
    <xsl:param name="ParentID"/>
    <xsl:param name="qPosition"/>
    <div class="questionaireDiv questionaireMatrix questionaire_{$qPosition}" id="questionaire_q_Matrix_{RMID}">
      <div class="matrixTable">
        <div class="matrixTableRow">
          <xsl:for-each select="../ResponseForMatrix[RMID=$ParentID and (ResponseFieldColName!='' and ResponseFieldColName!=' ')]">
            <xsl:if test="ISCellA1Added='false' and position()=1">
              <div class="matrixTableCell">&#160;</div>
            </xsl:if>
            <div class="matrixTableCell">
              <xsl:value-of select="ResponseFieldColName" disable-output-escaping="yes"/>&#160;
            </div>
          </xsl:for-each>
        </div>

        <xsl:for-each select="../ResponseForMatrix[RMID=$ParentID and ResponseFieldRowName!='']">
          <xsl:variable name="matrixRowID">
            <xsl:value-of select="RMatrixID" disable-output-escaping="yes"/>
          </xsl:variable>
          <div class="matrixTableRow">
            <div class="matrixTableCell">
              <xsl:value-of select="ResponseFieldRowName" disable-output-escaping="yes"/>
            </div>
            <xsl:for-each select="../ResponseForMatrix[RMID=$ParentID and (ResponseFieldColName!='' and ResponseFieldColName!=' ')]">
              <xsl:variable name="matrixColumnID">
                <xsl:value-of select="RMatrixID" disable-output-escaping="yes"/>
              </xsl:variable>
              <xsl:variable name="matrixSelected">
                <xsl:call-template name="childMatrixAnswers">
                  <xsl:with-param name="ParentID">
                    <xsl:value-of select="RMID"/>
                  </xsl:with-param>
                  <xsl:with-param name="matrixRowID">
                    <xsl:value-of select="$matrixRowID"/>
                  </xsl:with-param>
                  <xsl:with-param name="matrixColumnID">
                    <xsl:value-of select="$matrixColumnID"/>
                  </xsl:with-param>
                </xsl:call-template>
              </xsl:variable>
              <xsl:if test="(ISCellA1Added='true' and position()>1) or (ISCellA1Added='false')">
                <div class="matrixTableCell">
                  <xsl:if test="ISSingleChoiceEnabled='false'">
                    <input type="radio" id="questionaire_matrix_ctrl_{RMID}_{$matrixRowID}_{RMatrixID}" name="questionaire_matrix_group_{$matrixRowID}" value="{$matrixRowID}_{RMatrixID}"  onchange="responseChanged();">
                      <xsl:if test="$matrixSelected=1">
                        <xsl:attribute name="checked">
                        </xsl:attribute>
                      </xsl:if>
                    </input>
                    <label class="matrixTableLabel" for="questionaire_matrix_ctrl_{RMID}_{$matrixRowID}_{RMatrixID}">
                      <xsl:value-of select="ResponseFieldColName" disable-output-escaping="yes" />
                    </label>
                  </xsl:if>
                  <xsl:if test="ISSingleChoiceEnabled='true'">
                    <input type="checkbox" id="questionaire_matrix_ctrl_{RMID}_{$matrixRowID}_{RMatrixID}" name="questionaire_matrix_group_{$matrixRowID}" value="{$matrixRowID}_{RMatrixID}" onchange="responseChanged();">
                      <xsl:if test="$matrixSelected=1">
                        <xsl:attribute name="checked">
                        </xsl:attribute>
                      </xsl:if>
                    </input>
                    <label class="matrixTableLabel" for="questionaire_matrix_ctrl_{RMID}_{$matrixRowID}_{RMatrixID}">
                      <xsl:value-of select="ResponseFieldColName" disable-output-escaping="yes" />
                    </label>
                  </xsl:if>
                </div>
              </xsl:if>
            </xsl:for-each>
          </div>
        </xsl:for-each>
      </div>
      <xsl:for-each select="../ResponseForMatrix[RMID=$ParentID]">
        <xsl:if test="ISTextAreaAdded='true' and position()=1">
          <xsl:variable name="matrixComment">
            <xsl:call-template name="childMatrixAnswersComments">
              <xsl:with-param name="ParentID">
                <xsl:value-of select="RMID"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:variable>
          <br/>
          <label class="questionaireLabel">
            <xsl:value-of select="ResponseFieldAddTextAreaLabel" disable-output-escaping="yes"/>
          </label>
          <div name="questionaire_txt_MatrixOther_{RMatrixID}_{RMID}" rows="2" cols="20" id="questionaire_txt_MatrixOther_{RMatrixID}_{RMID}" class="qBodyTextArea ConsultEditor" onchange="responseChanged();" style="height: 100%;">
            <xsl:copy-of select="$matrixComment" />&#160;
          </div>
        </xsl:if>
      </xsl:for-each>
    </div>
  </xsl:template>

  <xsl:template name="childMatrixAnswersDivs">
    <xsl:param name="ParentID"/>
    <xsl:param name="matrixRowID"/>
    <xsl:param name="matrixColumnID"/>
    <xsl:for-each select="../MatrixAnswers[ResponseField_Master_ID=$ParentID and ResponseField_Child_ID=$matrixRowID and ResponseField_Child2_ID=$matrixColumnID]">
      <xsl:value-of select="Answer" disable-output-escaping="yes"/>
    </xsl:for-each>
  </xsl:template>


  <xsl:template name="childMatrixAnswersComments">
    <xsl:param name="ParentID"/>
    <xsl:for-each select="../MatrixAnswers[ResponseField_Master_ID=$ParentID and Is_Text_Area='true']">
      <xsl:value-of select="Answer" disable-output-escaping="yes"/>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>