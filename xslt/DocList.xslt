<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="NewDataSet">
		<div style="margin-left:35px; margin-top:20px;">
			<table class="borders full fixed wflTable">
				<xsl:for-each select="Table">
					<xsl:sort select="Precedence" order="ascending"/>
					<tr>
						<td class="borders wflTable1">
                            <a href="{documentAccessUrl}" target="_blank"><xsl:value-of select="documentName" disable-output-escaping="yes"/></a>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</div>
	</xsl:template>
</xsl:stylesheet>
